# Lost

## About
"Lost is a game that I started in September 2015 build from pure java and my own knowledge. Most of the games I (attempt) to make I don't finish (apart from the few) and so this was a target, I wanted to finish it! This is just that, a game that is in progress, a game that will get as far it will go."

In Lost you start with nothing and gain an inventory to escape from the loneliness ness of the world that you find your self in. However there a those who wish you harm and so you must equip yourself will weapons and tools and battle for your freedom.

##### View the website on [roryclaasen.co.nf](http://roryclaasen.co.nf/lost.html).

## Notes
This project now uses [Slick2D](http://slick.ninjacave.com/) and so if you would like to use it you will have to setup it in your workspace.
Please note that although the game will run on java 7 please use [java 8](https://www.java.com/en/download/) for the best result and to stop errors.
Any error that does happen please include your latest.log file that will be saved in the data folder!

## Data and Stats
[![Build Status](https://travis-ci.org/GOGO98901/Lost.svg)](https://travis-ci.org/GOGO98901/Lost) [![Stories in Ready](https://badge.waffle.io/GOGO98901/Lost.svg?label=ready&title=Issues Ready)](http://waffle.io/GOGO98901/Lost) [![Stories in progress](https://badge.waffle.io/GOGO98901/Lost.svg?label=in progress&title=Issues In progress)](http://waffle.io/GOGO98901/Lost)

[![Throughput Graph](https://graphs.waffle.io/GOGO98901/Lost/throughput.svg)](https://waffle.io/GOGO98901/Lost/metrics)
