# Lost
## Be Like Me
Here are the arguments that I pass through when I run/test [Lost](http://roryclaasen.co.nf/lost.html). If you want to run  from the command line or in an IDE (Eclipse etc.) or something

### For Lost
#### Java Arguments
```
-dev
```
#### VM Arguments
```
-DrunInEclipse=true
```
### For Lost-Editor
#### Java Arguments
```
	p=(Insert Path to workspace)\Lost\data\save s=level1
```
#### VM Arguments
```
```
