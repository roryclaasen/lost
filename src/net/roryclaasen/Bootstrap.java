package net.roryclaasen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;

import net.gogo98901.log.Level;
import net.gogo98901.log.Log;
import net.gogo98901.popup.Dialogs;
import net.gogo98901.reader.StaticReader;
import net.gogo98901.util.Data;
import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.toolbox.Arguments;
import net.roryclaasen.lost.toolbox.list.Files;
import net.roryclaasen.lost.toolbox.list.Strings;
import net.roryclaasen.lost.window.DisplayWindow;

import org.lwjgl.Sys;

//import net.roryclaasen.lost.window.Splash;

public class Bootstrap {

	private static long startTime = System.nanoTime(), launchTime;
	private static double launchInSeconds;

	private static String hash;
	private static int commitNumber;

	// private static Splash splash = new Splash("assets/logo/roryclaasen-back.png");

	public static void main(String[] args) {
		Log.setUseDate(false);
		Log.info("Program started");
		Log.setSave(true);
		Log.info(Strings._title + " by " + Strings._author);
		Log.info("Build: " + Strings._build);
		Arguments.check(args);
		setSystemProps();
		Log.info("-----------------------------------------------------------------------");
		printSystemData();
		Log.info("-----------------------------------------------------------------------");
		printProgramData();
		Log.info("-----------------------------------------------------------------------");
		if (Arguments.isDeveloper()) {
			Log.info("Running Developer.bat");
			try {
				Process process = Runtime.getRuntime().exec("developer.bat game");
				Log.info("Developer.bat\n\n" + getOutputOfProcess(process, false) + "\n");
			} catch (Exception e) {
				Log.info("Developer.bat Falied");
			}
		}
		try {
			StaticReader.setDoPrint(false);
			hash = StaticReader.read("version", true).replaceAll(System.getProperty("line.separator"), "");
			commitNumber = Integer.parseInt(StaticReader.read("number", true).replaceAll(System.getProperty("line.separator"), ""));
			StaticReader.setDoPrint(true);
		} catch (Exception e) {

		}
		Log.info("LAST HASH: " + hash);
		Log.info("   COMMIT: " + commitNumber);
		Log.info("-----------------------------------------------------------------------");
		if (init()) {
			Log.info("Pre Game Initialization... OKAY");
			start();
		} else {
			Log.severe("Pre Game Initialization... FAILED");
			Log.severe("Terminated");
		}
	}

	public static void setSystemProps() {
		String extention = null;
		if (OSValidator.isWindows()) extention = "windows";
		if (OSValidator.isMac()) extention = "macos";
		if (OSValidator.isUnix()) extention = "linux";
		if (OSValidator.isSolaris()) extention = "solaris";
		if (extention == null) {
			Dialogs.warning("This opperating system is not supported", Strings._title + " warning");
			Log.info("Opperating system not supprted!");
			Log.info("Terminated");
		}
		try {
			if (Arguments.inEclipse()) {
				System.setProperty("java.library.path", "lib");
				System.setProperty("org.lwjgl.librarypath", new File("lib/native/" + extention).getAbsolutePath());
			} else {
				if (Arguments.isBuildStart()) {
					System.setProperty("java.library.path", "build/jar/");
					System.setProperty("org.lwjgl.librarypath", new File("build/jar/native/" + extention).getAbsolutePath());

					ClassPathHacker.addFile("build/jar/lwjgl.jar");
					ClassPathHacker.addFile("build/jar/slick.jar");
				} else {
					System.setProperty("java.library.path", "");
					System.setProperty("org.lwjgl.librarypath", new File("native/" + extention).getAbsolutePath());

					ClassPathHacker.addFile("lwjgl.jar");
					ClassPathHacker.addFile("slick.jar");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.info("Terminated");
		}
		Log.info("System properties are now set up for " + Data.getOsName());
	}

	private static void carryOutFirstChecks() {
		Log.info("Starting first time checks");
		File checks = Files.checks;
		if (!checks.exists()) {
			boolean aboveOneSeven;
			{
				if (isAboveOneSeven()) {
					aboveOneSeven = true;
				} else {
					aboveOneSeven = false;
					Dialogs.warning("You are running java " + System.getProperty("java.vm.specification.version") + " but the recommended is 1.8.\nSome Features have been disabled", Strings._title + " warning");
				}
			}
			try {
				FileWriter file = new FileWriter(checks);
				file.write("Java Okay? " + aboveOneSeven);
				file.flush();
				file.close();
				Log.info("[First Time Check] File saved");
			} catch (IOException e) {
				Log.severe("[First Time Check] File failed to save");
				Log.stackTrace(e);
			}
		}
		Log.info("Checks allready carried out"/* ... Probably */);
	}

	private static boolean init() {
		try {
			Data.setDefultLookAndFeel();
		} catch (Exception e) {
			Log.stackTrace(Level.SEVERE, e);
		}
		carryOutFirstChecks();
		if (!isAboveOneSeven()) {
			Log.warn("Java is bellow 1.8 and some features may be disabled");

		}
		if (!isJavaFx()) {
			try {
				Log.info("Attempting to fix javafx import");
				ClassPathHacker.addFile(System.getProperty("java.home") + "\\lib\\jfxrt.jar");
				Log.info("Success");
			} catch (IOException e1) {
				Log.warn("Failed, some features will be disabled");
			}
			if (isJavaFx()) Log.info("javafx has been added to classpath");
			else Log.warn("javafx could not be added to classpath. Failed, some features will be disabled");
		} else Log.info("Skiping JavaFX import fix");
		if (Files.saveLevel1Folder.exists()) {
			// TODO check individual files
		} else {
			Dialogs.error("No level file found. Re-download from the website!", Strings._title + " error");
			return false;
		}
		return true;
	}

	private static void start() {
		launchTime = System.nanoTime() - startTime;
		launchInSeconds = (double) launchTime / 1000000000.0;
		Log.info("Boot sequence compleated in " + launchInSeconds + " seconds");
		
		Log.info("Starting game");
		if (!System.getProperty("user.name").toLowerCase().equals("travis")) {
			Files.createPaths();
			DisplayWindow window = new DisplayWindow();
			GameThread game;
			window.init((game = new GameThread(window)));
			game.init();
			game.start();
			window.start();
		} else {
			Log.info("Stopping game");
			Log.info("Travis detected!");
			Log.info("Terminated");
		}
	}

	public static String getSystemData() {
		String text = "";
		try {
			text += "        Workstation: " + InetAddress.getLocalHost().getHostName() + "\n";
		} catch (UnknownHostException e) {
			text += "        Workstation: UNKNOWN\n";
		}
		text += "               Name: " + System.getProperty("user.name") + "\n";
		text += "   Operating System: " + System.getProperty("os.name") + " (" + System.getProperty("os.version") + ")\n";
		text += "System Architecture: " + System.getProperty("os.arch") + "\n";
		text += "        Java Vendor: " + System.getProperty("java.vendor") + "\n";
		text += "       Java Version: " + System.getProperty("java.version") + "\n";
		// text += "     Java VM Vendor: " + System.getProperty("java.vm.specification.vendor") + "\n";
		text += "    Java VM Version: " + System.getProperty("java.vm.specification.version") + "\n";
		text += "    Java Class Path: " + System.getProperty("java.class.path") + "\n";
		text += "  Working Directory: " + System.getProperty("user.dir") + "\n";
		text += "              Cores: " + Runtime.getRuntime().availableProcessors() + "\n";
		text += "Free memory (bytes): " + Runtime.getRuntime().freeMemory() + "\n";
		text += "   Free memory (MB): " + (((double) Runtime.getRuntime().freeMemory() / 1024) / 1024) + "\n";
		return text;
	}

	public static String getProgramData() {
		String text = "";
		try {
			text += "     LWGJL Location: " + System.getProperty("org.lwjgl.librarypath") + "\n";
			text += "      LWGJL Version: " + Sys.getVersion() + "\n";
			// TODO Add anymore data that could help debugging
		} catch (Exception e) {
			text += "\n            ERROR: " + e + "\n";
		}
		return text;
	}

	public static void printSystemData() {
		String[] text = getSystemData().split("\n");
		for (String line : text) {
			Log.info(" System] " + line);
		}
	}

	public static void printProgramData() {
		String[] text = getProgramData().split("\n");
		for (String line : text) {
			Log.info("Program] " + line);
		}
	}

	public static void hideSplash() {
		// splash.dispose();
	}

	public static String getHashVersion() {
		return hash;
	}

	public static int getCommitNumber() {
		return commitNumber + 1;// The +1 is so that when committed the value will match the build
	}

	public static String getOutputOfProcess(Process proc, boolean print) throws IOException {
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		String s = null;
		String data = null;
		while ((s = stdInput.readLine()) != null) {
			if (print) Log.info(s);
			if (data == null) data = s;
			else data += "\n" + s;
		}
		return data;
	}

	public static String getErrorOfProcess(Process proc, boolean print) throws IOException {
		BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
		String s = null;
		String data = null;
		while ((s = stdError.readLine()) != null) {
			if (print) Log.warn(s);
			if (data == null) data = s;
			else data += "\n" + s;
		}
		return data;
	}

	public static boolean isJavaFx() {
		try {
			Class.forName("javafx.embed.swing.JFXPanel");
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isAboveOneSeven() {
		if (Double.parseDouble(System.getProperty("java.vm.specification.version")) > 1.7) return true;
		return false;
	}
}
