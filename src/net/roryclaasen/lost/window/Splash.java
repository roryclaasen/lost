package net.roryclaasen.lost.window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;

import net.gogo98901.util.Loader;
import net.roryclaasen.lost.toolbox.list.Strings;

public class Splash {
	private JDialog frame;
	private BufferedImage image;

	public Splash(String imagePath) {
		try {
			image = Loader.loadBufferedImagefromJar(imagePath);
			if (image != null) {
				init();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void init() {
		frame = new JDialog();
		frame.setAlwaysOnTop(true);
		frame.setUndecorated(true);
		frame.setTitle(Strings._title);
		frame.setSize(DisplayWindow.getWidth(), DisplayWindow.getHeight());
		frame.setBackground(new Color(0, 0, 0, 0));
		JLabel splash = new JLabel();
		splash.setIcon(new ImageIcon(image));
		frame.add(splash, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public void hide() {
		frame.setVisible(false);
	}

	public void dispose() {
		frame.dispose();
	}
}
