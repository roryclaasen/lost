package net.roryclaasen.lost.window;

import java.awt.event.KeyEvent;

import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.event.Key.KeyEventListener;
import net.roryclaasen.lost.handler.KeyPress;
import net.roryclaasen.lost.state.States;
import net.roryclaasen.lost.toolbox.Arguments;

import org.newdawn.slick.GameContainer;

public class GameCanvas {

	private GameThread game;

	@SuppressWarnings("unused")
	private int width, height;

	public States states;

	private KeyPress bounds;

	public GameCanvas(GameThread game, int width, int height) {
		this.game = game;
		this.width = width;
		this.height = height;

		states = new States(game, width, height);
	}

	public void init() {
		bounds = new KeyPress(KeyEvent.VK_F10);
		bounds.addListener(new KeyEventListener() {

			@Override
			public void keyPress(net.roryclaasen.lost.event.Key.KeyEvent evt) {}

			@Override
			public void keyRelease(net.roryclaasen.lost.event.Key.KeyEvent evt) {
				Arguments.toggleBounds();
			}
		});
	}

	int reLoad = 0;

	public void update(GameContainer container, int delta) {
		game.handler.keyboard.update();
		game.handler.mouse.update();
		game.soundManager.update();

		masterUpdate();
	}

	private void masterUpdate() {
		bounds.update();
	}
}
