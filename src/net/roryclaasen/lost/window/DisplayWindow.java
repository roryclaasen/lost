package net.roryclaasen.lost.window;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.SwingUtilities;

import net.gogo98901.log.Log;
import net.gogo98901.util.Loader;
import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.toolbox.list.Strings;
import net.roryclaasen.lost.toolbox.ui.VideoPlayer;

import org.newdawn.slick.CanvasGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

public class DisplayWindow {

	private static JFrame frame;
	private static JLayeredPane lPane;

	private static CanvasGameContainer gameContaniner;

	private static final double mul = 1.4;
	private static final int width = (int) (720 * mul), height = (int) (405 * mul);

	public void init(GameThread game) {
		frame = new JFrame();
		initFrame();
		try {
			gameContaniner = new CanvasGameContainer(game);
			gameContaniner.setBounds(0, 0, width, height);
			lPane.add(gameContaniner);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	private void initFrame() {
		frame.setPreferredSize(new Dimension(width + 6, height + 29));
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setTitle(Strings._title + " by " + Strings._author);
		frame.setResizable(false);
		try {
			frame.setIconImage(Loader.loadImagefromJar("assets/textures/player.png").getSubimage(32, 0, 32, 32));
		} catch (Exception e) {
		}
		frame.setBackground(Color.BLACK);

		Container pane = frame.getContentPane();
		pane.setBackground(frame.getBackground());
		lPane = new JLayeredPane();
		lPane.setBounds(0, 0, width, height);
		lPane.setBackground(frame.getBackground());
		pane.add(lPane);
		setVisible(true);
	}

	public static void setUpGameContainer(GameContainer game) {
		// game.setVSync(true);
		game.setShowFPS(true);
		game.setAlwaysRender(false);
	}

	public void start() {
		frame.setVisible(true);
		try {
			gameContaniner.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	public static void setVideo(VideoPlayer player) {
		// gameContaniner.setVisible(false);
		gameContaniner.setFocusable(false);
		try {
			lPane.add(player, new Integer(100));
		} catch (Exception e) {
			Log.stackTrace(e);
		}
	}

	public static void removeVideo(final VideoPlayer player) {
		// gameContaniner.setVisible(true);
		gameContaniner.setFocusable(true);
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				try {
					lPane.remove(player);
					lPane.invalidate();
					lPane.validate();
					lPane.repaint();
				} catch (Exception e) {
					Log.stackTrace(e);
				}
			}
		});
	}

	public static void setVisible(Boolean arg0) {
		frame.setVisible(arg0);
	}

	public static final int getWidth() {
		return width;
	}

	public static final int getHeight() {
		return height;
	}

	public static CanvasGameContainer getGameContaniner() {
		return gameContaniner;
	}

	public static JFrame getFrame() {
		return frame;
	}
}
