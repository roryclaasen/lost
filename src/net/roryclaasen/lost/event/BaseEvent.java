package net.roryclaasen.lost.event;

import java.awt.Point;
import java.util.EventObject;

import net.roryclaasen.lost.handler.HandlerMouse;

public class BaseEvent extends EventObject {
	private static final long serialVersionUID = 1L;

	public BaseEvent(Object source) {
		super(source);
	}

	public Point getMousePoint() {
		return new Point(HandlerMouse.getX(), HandlerMouse.getY());
	}
}
