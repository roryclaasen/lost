package net.roryclaasen.lost.event.Slider;

import java.util.EventListener;

public interface SliderEventListener extends EventListener {
	public void sliderMoved(SliderEvent evt);
	public void sliderButtonReleased(SliderEvent evt);
}