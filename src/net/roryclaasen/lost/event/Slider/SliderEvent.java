package net.roryclaasen.lost.event.Slider;

import net.roryclaasen.lost.event.BaseEvent;
import net.roryclaasen.lost.graphics.ui.Slider;

public class SliderEvent extends BaseEvent {
	private static final long serialVersionUID = 1L;
	
	public SliderEvent(Object source) {
		super(source);
	}
	
	public int getPosition() {
		if (source instanceof Slider) {
			return ((Slider) source).getValue();
		}
		return -1;
	}
}