package net.roryclaasen.lost.event.Block;

import java.util.EventListener;

public interface BlockListener extends EventListener {
	public void blockHit(BlockEvent evt);
	public void blockDestroyed(BlockEvent evt);
}
