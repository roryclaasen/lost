package net.roryclaasen.lost.event.Key;

import java.util.EventListener;

public interface KeyEventListener extends EventListener {
	public void keyPress(KeyEvent evt);
	public void keyRelease(KeyEvent evt);
}