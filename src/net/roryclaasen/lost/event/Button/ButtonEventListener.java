package net.roryclaasen.lost.event.Button;

import java.util.EventListener;

public interface ButtonEventListener extends EventListener {
	public void buttonClick(ButtonEvent evt);
	public void buttonPress(ButtonEvent evt);
	public void buttonDragged(ButtonEvent evt);
}
