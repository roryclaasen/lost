package net.roryclaasen.lost.options;

public class OptionFloat extends Option {

	public OptionFloat(String name, float data) {
		super(Type.FLOAT,name, data);
	}

	public float get() {
		return Float.parseFloat(getAsString());
	}
}
