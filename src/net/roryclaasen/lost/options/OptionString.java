package net.roryclaasen.lost.options;

public class OptionString extends Option {

	public OptionString(String name, String data) {
		super(Type.STRING, name, data);
	}

	public String get() {
		return getAsString();
	}
}
