package net.roryclaasen.lost.options;

public class OptionInteger extends Option {

	public OptionInteger(String name, int data) {
		super(Type.INTEGER, name, data);
	}

	public int get() {
		return Integer.parseInt(getAsString());
	}
}
