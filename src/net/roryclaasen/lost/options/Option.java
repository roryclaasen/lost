package net.roryclaasen.lost.options;


public class Option {

	protected String name;
	protected Object data;

	public enum Type {
		BOOLEAN, FLOAT, INTEGER, STRING
	}

	protected final Type type;

	public Option(Type type, String name, Object data) {
		this.type = type;
		this.name = name;
		this.data = data;
		Options.getOptions().add(this);
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public String getAsString() {
		return data.toString();
	}

	public String getName() {
		return name;
	}
}
