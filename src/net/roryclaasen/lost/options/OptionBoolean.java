package net.roryclaasen.lost.options;

public class OptionBoolean extends Option {

	public OptionBoolean(String name, boolean data) {
		super(Type.BOOLEAN, name, data);
	}

	public boolean get() {
		return Boolean.parseBoolean(getAsString());
	}
}
