package net.roryclaasen.lost.options;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.gogo98901.log.Log;
import net.gogo98901.reader.StaticReader;
import net.roryclaasen.lost.toolbox.list.Files;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Options {
	private JSONParser parser = new JSONParser();

	private static List<Option> options = new ArrayList<Option>();
	
	public static OptionBoolean itemCount = new OptionBoolean("showItemCount", false);
	public static OptionBoolean healthAutoHide = new OptionBoolean("healthBarHideAuto", true);
	public static OptionInteger messgaeTextOpacity = new OptionInteger("messageTextopacity", 200);
	public static OptionBoolean allowMouseAttacking = new OptionBoolean("playerMouseAttack", true);

	public static OptionBoolean showParticles = new OptionBoolean("showParticles", true);

	public static OptionBoolean doMusic = new OptionBoolean("doMusic", true);
	public static OptionBoolean doSounds = new OptionBoolean("doSounds", true);
	
	public static OptionInteger playerShaddowSize = new OptionInteger("playerShaddowSize", 500);
	public static OptionBoolean mobsCanMove = new OptionBoolean("masterMobAllowMove", true);

	public Options() {
		if (!Files.optionFile.exists()) save();
		load();
	}

	public void load() {
		try {
			JSONObject jsonFile = (JSONObject) parser.parse(StaticReader.read(Files.optionFile.getAbsolutePath().toString(), false));
			for (Option option : options) {
				String key = option.getName();
				try {
					Object data = jsonFile.get(key);
					if (data != null) option.setData(data);
				} catch (Exception e) {
					Log.warn("[Options] Loading error with key '" + key + "', this will be fixed on save: " + e);
				}
			}
		} catch (ParseException e) {
			Log.warn(e);
		}
	}

	@SuppressWarnings("unchecked")
	public void save() {
		JSONObject jsonFile = new JSONObject();
		for (Option option : options) {
			jsonFile.put(option.getName(), option.getData());
		}
		try {
			FileWriter file = new FileWriter(Files.optionFile);
			file.write(jsonFile.toJSONString());
			file.flush();
			file.close();
			Log.info("[Options] File saved");
		} catch (IOException e) {
			Log.severe("[Options] File failed to save");
			Log.stackTrace(e);
		}
	}

	public static List<Option> getOptions() {
		return options;
	}
}
