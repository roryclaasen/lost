package net.roryclaasen.lost.graphics.level;

import net.roryclaasen.lost.level.entity.Mob;
import net.roryclaasen.lost.level.entity.mob.player.Player;
import net.roryclaasen.lost.options.Options;
import net.roryclaasen.lost.toolbox.list.Colors;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class HealthBar {

	private Mob mob;

	private int width = 38, height = 5;
	private int opacity = 150, opacityDefult = opacity;

	private int time;
	private final int MAX_TIME = 60;

	private double oldHealth;

	private boolean visible, fade;

	public HealthBar(Mob mob) {
		this.mob = mob;
		oldHealth = mob.getHealth();
	}

	public void render(Graphics g, int x_off, int y_off) {
		if (visible) {
			Color c = g.getColor();
			int x = x_off + mob.getX() + (mob.getSprite().getWidth() / 2) - (width / 2) - 1;
			int y = y_off + mob.getY() - height - 5;
			double barWidth = (mob.getHealth() / mob.getMaxHealth()) * width;
			g.setColor(Colors.setOpacity(Colors.healthFill, opacity));
			g.fillRect(x, y, (int) barWidth, height);
			g.setColor(Colors.setOpacity(Colors.healthBorder, opacity));
			g.fillRect(x + (int) barWidth, y, width - (int) barWidth, height);
			g.setColor(Colors.setOpacity(Colors.healthBorder, opacity));
			g.drawRect(x, y, width, height);
			g.setColor(c);
		}
	}

	public void update() {
		if (!(mob instanceof Player)) {
			if (oldHealth != mob.getHealth()) show();
			if (Options.healthAutoHide.get()) {
				if (visible) {
					time++;
					if (time > MAX_TIME) fade = true;
				}
				if (fade) {
					opacity--;
					if (opacity == 0) hide();
				}
			}
		}
		oldHealth = mob.getHealth();
	}

	public void show() {
		visible = true;
		opacity = opacityDefult;
		time = 0;
	}

	public void hide() {
		visible = false;
		opacity = opacityDefult;
		time = 0;
		fade = false;
	}

	public void hideFade() {
		fade = true;
	}
}
