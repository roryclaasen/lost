package net.roryclaasen.lost.graphics.ui;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.toolbox.list.Colors;
import net.roryclaasen.lost.toolbox.list.Fonts;
import net.roryclaasen.lost.window.DisplayWindow;

import org.newdawn.slick.Graphics;

public class UserInterface {

	private Level level;
	private List<MessageUI> messages = new ArrayList<MessageUI>();
	private InventoryUI inventory;

	private final int MAX_MESSAGES = 10;

	public UserInterface(Level level) {
		this.level = level;
		inventory = new InventoryUI(level);
		inventory.load();
	}

	public void init() {
		inventory.setPlayer();
	}

	public void update() {
		if (inventory != null) inventory.update();
		for (int i = 0; i < messages.size(); i++) {
			MessageUI message = messages.get(i);
			if (message.remove()) {
				messages.remove(i);
				continue;
			}
			message.setPosition(i);
			message.update();
		}
	}

	public void render(Graphics g) {
		g.setFont(Fonts.square24);

		if (inventory != null) inventory.render(g);

		Rectangle b = inventory.getBounds();
		double barWidth = (level.getPlayer().getHealth() / level.getPlayer().getMaxHealth()) * (DisplayWindow.getWidth() - (b.x + b.width + 10) - 10);
		g.setColor(Colors.setOpacity(Colors.healthFill, 130));
		g.fillRect(b.x + b.width + 10, b.y + ((int) (b.height / 6)), (int) barWidth, (int) (b.height / 1.5));
		g.setColor(Colors.setOpacity(Colors.healthBorder, 130));
		g.fillRect(b.x + b.width + 10 + (int) barWidth, b.y + ((int) (b.height / 6)), DisplayWindow.getWidth() - (b.x + b.width + 10) - 10 - (int) barWidth, (int) (b.height / 1.5));
		g.setColor(Colors.setOpacity(Colors.healthBorder, 200));
		g.drawRect(b.x + b.width + 10, b.y + ((int) (b.height / 6)), DisplayWindow.getWidth() - (b.x + b.width + 10) - 10, (int) (b.height / 1.5));
		Fonts.drawCenteredString(g, (int) level.getPlayer().getHealth() + "/" + (int) level.getPlayer().getMaxHealth(), b.x + b.width + 10, b.y + ((int) (b.height / 6)), DisplayWindow.getWidth() - (b.x + b.width + 10) - 10, (int) (b.height / 1.5));

		for (MessageUI mess : messages) {
			mess.render(g);
		}
	}

	public InventoryUI getInv() {
		return inventory;
	}

	public void newMessage(String message, double time) {
		messages.add(new MessageUI(message, time));
		while (messages.size() >= MAX_MESSAGES) {
			messages.remove(0);
		}
	}
}
