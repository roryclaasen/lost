package net.roryclaasen.lost.graphics.ui;

import java.util.ArrayList;
import java.util.List;

import net.roryclaasen.lost.window.DisplayWindow;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class BackgroundImage extends uiBaseObject {

	private List<Image> images = new ArrayList<Image>();

	private int time, length;

	private Image current, old;

	private Color background;

	private boolean fadeOut = false;

	private float opacity = 1F;

	public BackgroundImage(List<Image> images) {
		super(0, 0, DisplayWindow.getWidth(), DisplayWindow.getHeight());
		this.images = images;
		nextImage();
	}

	public BackgroundImage(int x, int y, int width, int height, List<Image> images) {
		super(x, y, width, height);
		this.images = images;
		nextImage();
	}

	@Override
	public void render(Graphics g) {
		if (background != null) {
			g.setColor(background);
			g.fillRect(x, y, width, height);
		}
		if (current != null) {
			current.draw(x, y, width, height);
		}
		if (old != null) {
			old.draw(x, y, width, height);
		}
	}

	@Override
	public void update() {
		if (time > length) {
			nextImage();
		}
		time++;
		if (fadeOut) {
			if (old == null) {
				fadeOut = false;
				return;
			}
			opacity -= 0.0075F;
			old.setAlpha(opacity);
			if (opacity <= 0) {
				old = null;
			}
		}
	}

	public void nextImage() {
		time = 0;
		old = current;
		int index = images.indexOf(current) + 1;
		if (index >= images.size()) index = 0;
		current = images.get(index);
		fadeOut = true;
		opacity = 1F;
		current.setAlpha(1F);
		if (old != null) old.setAlpha(opacity);
	}

	public void reset() {
		time = 0;
		old = null;
		int index = images.indexOf(current) + 1;
		if (index >= images.size()) index = 0;
		current = images.get(index);
		fadeOut = false;
		opacity = 1F;
	}

	public void setImages(List<Image> images) {
		this.images.clear();
		this.images = images;
	}

	public void addImage(Image image) {
		images.add(image);
	}

	public List<Image> getImages() {
		return images;
	}

	public void setLength(double seconds) {
		length = (int) Math.ceil(60 * seconds);
	}

	public int getLength() {
		return length;
	}

	public void setBackgroundColor(Color color) {
		this.background = color;
	}
}
