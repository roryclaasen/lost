package net.roryclaasen.lost.graphics.ui;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.EventListener;

import org.newdawn.slick.Graphics;

public abstract class uiBaseObject {

	protected EventListener listener;

	protected boolean overrideCall = false;

	protected String text;
	protected int x, y, width, height;

	public uiBaseObject(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public uiBaseObject(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void setBounds(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public abstract void render(Graphics g);

	public abstract void update();

	/*
	 * public void addEventListener(EventListener listener) {
	 * this.listener = listener;
	 * }
	 */

	public void removeEventListener() {
		listener = null;
	}

	public boolean hasListener() {
		return (listener != null);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Point getLocation() {
		return new Point(x, y);
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}
}
