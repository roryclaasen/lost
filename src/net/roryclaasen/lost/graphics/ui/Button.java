package net.roryclaasen.lost.graphics.ui;

import net.roryclaasen.lost.event.Button.ButtonEvent;
import net.roryclaasen.lost.event.Button.ButtonEventListener;
import net.roryclaasen.lost.handler.HandlerMouse;
import net.roryclaasen.lost.toolbox.Arguments;
import net.roryclaasen.lost.toolbox.list.Colors;
import net.roryclaasen.lost.toolbox.list.Fonts;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;

public class Button extends uiBaseObject {

	private int press;

	private boolean output = false, check, background = true;

	private final int NORMAL = 0;
	private final int HOVER = 1;
	private final int CLICK = 2;
	private int state = NORMAL;

	private float size = 30F;

	private Color border;

	private Font font = Fonts.square32;

	public Button(String text, int x, int y) {
		super(x, y, 200, 40);
		this.text = text;
	}

	@Override
	public void update() {
		int mx = HandlerMouse.getX();
		int my = HandlerMouse.getY();
		if (HandlerMouse.getButton() == HandlerMouse.BTN_LEFT && ((mx < x || mx > x + width || my < y || my > y + height))) {
			check = false;
		} else {
			if (HandlerMouse.getButton() != HandlerMouse.BTN_LEFT) check = true;
		}
		if (check) {
			if (mx >= x && mx <= x + width && my >= y && my <= y + height) {
				state = HOVER;
				if (HandlerMouse.getButton() == HandlerMouse.BTN_LEFT) {
					if (press == 0) callPressListener();
					press++;
					state = CLICK;
					callDragListener();
				} else {
					output = false;
					if (press != 0) {
						output = true;
						if (!overrideCall) callClickListener();
						press = 0;
					}
				}
			} else {
				state = NORMAL;
				press = 0;
			}
		} else {
			state = NORMAL;
			press = 0;
		}
	}

	@Override
	public void render(Graphics g) {
		if (background) {
			if (border != null) {
				g.setColor(border);
				g.fillRect(x - 2, y - 2, width + 4, height + 4);
				g.fillRect(x, y, width + 4, height + 4);
			}
			g.setColor(Color.black);
			g.fillRect(x + 2, y + 2, width, height);

			g.setColor(Colors.buttonBack);
			if (state == HOVER) g.setColor(Colors.buttonHover);
			if (state == CLICK) g.setColor(Color.darkGray);
			g.fillRect(x, y, width, height);
		}
		if (text != null) {
			Fonts.drawCenteredString(g, text, font, x, y, width, height);
		}
		if (Arguments.showBounds()) {
			g.setColor(Colors.boxButton);
			g.drawRect(x, y, width, height);
		}
	}

	public void addEventListener(ButtonEventListener listener) {
		this.listener = listener;
	}

	public void callClickListener() {
		if (listener != null) ((ButtonEventListener) listener).buttonClick(new ButtonEvent(this));
	}

	public void callPressListener() {
		if (listener != null) ((ButtonEventListener) listener).buttonPress(new ButtonEvent(this));
	}

	public void callDragListener() {
		if (listener != null) ((ButtonEventListener) listener).buttonDragged(new ButtonEvent(this));
	}

	public void setBackgroundVisible(boolean background) {
		this.background = background;
	}

	public String getText() {
		return text;
	}

	public boolean getOutput() {
		return output;
	}

	public float getTextSize() {
		return size;
	}

	public void setTextSize(float size) {
		this.size = size;
	}

	public void setBorderColor(Color border) {
		this.border = border;
	}

	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
	}
}
