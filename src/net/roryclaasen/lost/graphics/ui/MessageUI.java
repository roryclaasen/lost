package net.roryclaasen.lost.graphics.ui;

import net.roryclaasen.lost.options.Options;
import net.roryclaasen.lost.toolbox.list.Fonts;

import org.newdawn.slick.Graphics;

public class MessageUI extends uiBaseObject {

	private static int startY = 18;
	private int distance = 16;
	private String text;
	private double fadeTime;

	private int time, fade = Options.messgaeTextOpacity.get();

	private boolean remove = false, slide = false;

	private int position = 0, yOffset = 0;

	public MessageUI(String text, double fadeTime) {
		super(5, startY);
		this.text = text;
		this.fadeTime = fadeTime;
	}

	@Override
	public void update() {
		time++;
		if (slide) {
			if (time % 2 == 0) yOffset--;
			if (yOffset <= 0) {
				yOffset = 0;
				time = 0;
				slide = false;
			}
		} else if (position == 0) {
			if (time / 60 > fadeTime) {
				fade -= 5;
			}
			if (fade <= 0) {
				fade = 0;
				remove = true;
			}
		}
	}

	@Override
	public void render(Graphics g) {
		Fonts.drawString(g, Fonts.square18, text, x, y + yOffset, fade);
	}

	public boolean remove() {
		return remove;
	}

	public void setPosition(int newPosition) {
		if (newPosition < position) {
			slide = true;
			fade = Options.messgaeTextOpacity.get();
			yOffset = distance;
		}
		position = newPosition;
		y = startY + (distance * position);
	}
}
