package net.roryclaasen.lost.graphics.ui;

public class ButtonToggle extends Button {

	private String[] values;
	private int defultValue, value;

	public ButtonToggle(String[] values, int x, int y, int defultValue) {
		super(values[defultValue], x, y);
		this.values = values;
		this.defultValue = defultValue;

		overrideCall = true;
	}

	@Override
	public void update() {
		super.update();
		if (getOutput()) {
			value++;
			if (value >= values.length) value = 0;
			callClickListener();
		}
		text = getCurrentValue();
	}

	public String getCurrentValue() {
		return values[value];
	}

	public String[] getValues() {
		return values;
	}

	public int getDefultValue() {
		return defultValue;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
