package net.roryclaasen.lost.graphics.ui;

import net.roryclaasen.lost.event.Button.ButtonEvent;
import net.roryclaasen.lost.event.Button.ButtonEventListener;
import net.roryclaasen.lost.event.Slider.SliderEvent;
import net.roryclaasen.lost.event.Slider.SliderEventListener;
import net.roryclaasen.lost.handler.HandlerMouse;
import net.roryclaasen.lost.toolbox.Arguments;
import net.roryclaasen.lost.toolbox.list.Colors;
import net.roryclaasen.lost.toolbox.list.Fonts;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Slider extends uiBaseObject {

	private final int MAX_VALUE;
	private int value, defultValue, fontOpacity = 255;

	private float size = 30F;
	private int mouseOffset;

	private int barWidth, barStartX, barOffset = 5;

	private int slideWidth = 10, slideHeight = 46;

	private boolean check = false;
	private String overirde;

	private Button slide;

	public Slider(int x, int y, int value, final int maxValue) {
		super(x, y, 300, 40);
		this.MAX_VALUE = maxValue;
		this.value = value;
		this.defultValue = value;

		this.barStartX = x + barOffset;
		this.barWidth = this.width - (barOffset * 2) - (slideWidth / 2);
		slide = new Button(null, barStartX, y - ((slideHeight - height) / 2));
		slide.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				callSliderButtonReleased();
			}

			@Override
			public void buttonPress(ButtonEvent evt) {
				mouseOffset = slide.getX() - evt.getMousePoint().x;
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {
				int value = (evt.getMousePoint().x + mouseOffset) - slide.getX();
				if (slide.getX() + value >= barStartX && slide.getX() + value <= barStartX + barWidth - slideWidth) {
					slide.setX(slide.getX() + value);
					callMoveListener();
				}
			}
		});
		slide.setBorderColor(Color.white);
		slide.setWidth(slideWidth);
		slide.setHeight(slideHeight);
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(x + 2, y + 2, width, height);
		g.setColor(Colors.buttonBack);
		g.fillRect(x, y, width, height);

		slide.render(g);
		String content = value + "/" + MAX_VALUE;
		if (overirde != null) content = overirde;
		Fonts.drawCenteredString(g, content, Fonts.square30, x, y, width, height, fontOpacity);

		if (Arguments.showBounds()) {
			g.setColor(Colors.boxButton);
			g.drawRect(x, y, width, height);
			g.setColor(Colors.boxButton);
			g.drawRect(barStartX, y, barWidth, height);
		}
	}

	@Override
	public void update() {
		int mx = HandlerMouse.getX();
		int my = HandlerMouse.getY();
		slide.update();
		value = (MAX_VALUE / barWidth) * (slide.getX() - barStartX);
		fontOpacity = 255;
		if (HandlerMouse.getButton() == HandlerMouse.BTN_LEFT && ((mx < x || mx > x + width || my < y || my > y + height))) {
			check = false;
		} else {
			if (HandlerMouse.getButton() != HandlerMouse.BTN_LEFT) check = true;
		}
		if (check) {
			if (mx >= x && mx <= x + width && my >= y && my <= y + height) {
				if (HandlerMouse.getButton() == HandlerMouse.BTN_LEFT) {
					fontOpacity = 150;
					int value = mx - (slide.getWidth() / 2);
					if (value >= barStartX && value <= barStartX + barWidth - (slideWidth / 2)) {
						slide.setX(value);
						callMoveListener();
					}
				}
			}
		}
	}

	public void addEventListener(SliderEventListener listener) {
		this.listener = listener;
	}

	public void callSliderButtonReleased() {
		if (listener != null) ((SliderEventListener) listener).sliderButtonReleased(new SliderEvent(this));
	}

	public void callMoveListener() {
		if (listener != null) ((SliderEventListener) listener).sliderMoved(new SliderEvent(this));
	}

	public void setTextOveride(String text) {
		this.overirde = text;
	}

	public void setValue(int value) {
		this.value = value;
		// value is set by '(MAX_VALUE / (width - (endOffsets *2))) * (slide.getX() - getX())'
		int pos = value / (MAX_VALUE / barWidth) + barStartX;
		if (pos > barStartX + barWidth - (slideWidth / 2)) pos = barStartX + barWidth - (slideWidth / 2);
		if (pos < barStartX) pos = barStartX;
		slide.setX(pos);
		callMoveListener();
	}

	public int getValue() {
		return value;
	}

	public int getDefultValue() {
		return defultValue;
	}

	public float getTextSize() {
		return size;
	}

	public void setTextSize(float size) {
		this.size = size;
	}
}
