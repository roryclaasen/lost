package net.roryclaasen.lost.graphics.ui;

import java.awt.Rectangle;

import net.gogo98901.log.Log;
import net.roryclaasen.lost.graphics.item.ItemStack;
import net.roryclaasen.lost.graphics.item.Items;
import net.roryclaasen.lost.handler.HandlerKeyboard;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.level.entity.mob.player.Inventory;
import net.roryclaasen.lost.level.entity.mob.player.Player;
import net.roryclaasen.lost.options.Options;
import net.roryclaasen.lost.toolbox.list.Fonts;
import net.roryclaasen.lost.window.DisplayWindow;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class InventoryUI {

	private Image[] slots = new Image[3];

	private Level level;
	private Player player;

	private ItemStack[] stacks;
	private int sel = 0;

	private Rectangle bounds;

	public InventoryUI(Level level) {
		this.level = level;
	}

	public void load() {
		try {
			Image img = new Image("assets/textures/gui.slots.png");
			slots[0] = img.getSubImage(0, 0, 36, 36);
			slots[1] = img.getSubImage(36, 0, 36, 36);
			slots[2] = img.getSubImage(72, 0, 36, 36);
		} catch (Exception e) {
			Log.stackTrace(net.gogo98901.log.Level.SEVERE, e);
			try {
				throw new Exception("Could not load up gui!");
			} catch (Exception e1) {
				Log.severe("Could not load up gui!");
			}
		}
	}

	public void setPlayer() {
		this.player = level.getPlayer();
		stacks = player.inv.getStacks().clone();

		bounds = new Rectangle(10, DisplayWindow.getHeight() - 49, 10 + (38 * stacks.length) + (2 * stacks.length), 38);
	}

	public void update() {
		stacks = player.inv.getStacks().clone();
		player.inv.setSelected(sel);
		int oldSel = sel;

		if (HandlerKeyboard.no1) sel = 0;
		if (HandlerKeyboard.no2) sel = 1;
		if (HandlerKeyboard.no3) sel = 2;
		if (HandlerKeyboard.no4) sel = 3;
		if (HandlerKeyboard.no5) sel = 4;
		if (player.inv.hasBackpack()) {
			if (HandlerKeyboard.no6) sel = 5;
			if (HandlerKeyboard.no7) sel = 6;
			if (HandlerKeyboard.no8) sel = 7;
			if (HandlerKeyboard.no9) sel = 8;
			if (HandlerKeyboard.no0) sel = 9;
		}
		if (HandlerKeyboard.ctrl) {
			if (oldSel != sel) move(oldSel, sel);
		}

		if (HandlerKeyboard.q) {
			player.inv.drop(sel);
		}
	}

	private void move(int oSel, int nSel) {
		if (nSel > Inventory.SLOTS_NO_BACKPACK && !player.inv.hasBackpack()) return;
		ItemStack pos1 = player.inv.getStacks()[oSel];
		ItemStack pos2 = player.inv.getStacks()[nSel];
		player.inv.getStacks()[oSel] = pos2;
		player.inv.getStacks()[nSel] = pos1;
	}

	public void render(Graphics g) {
		Color c = g.getColor();
		g.setColor(Color.white);
		for (int i = 0; i < stacks.length; i++) {
			Image img = slots[0];
			if (i == sel) img = slots[1];
			if (!player.inv.hasBackpack() && i >= Inventory.SLOTS_NO_BACKPACK) img = slots[2];
			img.draw(bounds.x + (41 * i), bounds.y, 38, 38);
			if (stacks[i] != null) {
				if (Items.getItem(stacks[i].getID()) != null) {
					Items.getItem(stacks[i].getID()).getSprite().getImage().draw(bounds.x + 2 + (41 * i), bounds.y + 4);
					Fonts.drawString(g, Fonts.square18, stacks[i].getSize() + "", bounds.x + 28 + (41 * i), bounds.y + 20);
					if (i == sel) {
						String s = stacks[i].getName();
						if (Options.itemCount.get()) s += " (" + stacks[i].getSize() + ")";
						Fonts.drawCenteredString(g, s, Fonts.square24, 0, DisplayWindow.getHeight() - 70, bounds.width, 20);
					}
				}
			}
		}
		g.setColor(c);
	}

	public void up(int steps) {
		sel += steps;
		if (player.inv.hasBackpack()) {
			if (sel >= Inventory.SLOTS_MAX) if (sel >= Inventory.SLOTS_MAX) sel = 0;
		} else {
			if (sel >= Inventory.SLOTS_NO_BACKPACK) sel = 0;
		}
	}

	public void down(int steps) {
		sel -= steps;
		if (sel < 0) {
			if (player.inv.hasBackpack()) sel = Inventory.SLOTS_MAX - 1;
			else sel = Inventory.SLOTS_NO_BACKPACK - 1;
		}
	}

	public Rectangle getBounds() {
		return bounds;
	}
}
