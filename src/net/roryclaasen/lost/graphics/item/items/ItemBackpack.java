package net.roryclaasen.lost.graphics.item.items;

import net.roryclaasen.lost.graphics.item.ItemBase;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;

public class ItemBackpack extends ItemBase {

	public ItemBackpack(SpriteSheet sheet, int id, int stackSize, String name) {
		super(sheet, id, stackSize, name);
	}

	@Override
	public void use() {

	}

	@Override
	public boolean consumable() {
		return false;
	}
}
