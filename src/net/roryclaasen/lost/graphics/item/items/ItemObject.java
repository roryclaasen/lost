package net.roryclaasen.lost.graphics.item.items;

import net.roryclaasen.lost.graphics.item.ItemBase;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;

public class ItemObject extends ItemBase {

	public ItemObject(SpriteSheet sheet, int id, int stackSize, String name) {
		super(sheet, id, stackSize, name);
	}

	public ItemObject(SpriteSheet sheet, int[] ids, int stackSize, String name) {
		super(sheet, ids[0], stackSize, name);
		for (int i = 1; i < ids.length; i++) {
			new ItemObject(sheet, ids[i], stackSize, name);
		}
	}

	@Override
	public void use() {
		
	}

	@Override
	public boolean consumable() {
		return false;
	}
}
