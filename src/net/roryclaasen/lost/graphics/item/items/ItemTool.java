package net.roryclaasen.lost.graphics.item.items;

import net.roryclaasen.lost.graphics.item.ItemBase;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;

public class ItemTool extends ItemBase {
	protected int durability = 100;

	public ItemTool(SpriteSheet sheet, int id, int stackSize, String name) {
		super(sheet, id, stackSize, name);
	}

	public ItemTool(SpriteSheet sheet, int[] ids, int stackSize, String name) {
		super(sheet, ids[0], stackSize, name);
		for (int i = 1; i < ids.length; i++) {
			new ItemTool(sheet, ids[i], stackSize, name);
		}
	}

	@Override
	public void use() {
		durability--;
		if (durability == 0) remove();
	}

	@Override
	public boolean consumable() {
		return false;
	}

	public boolean canDestroyBlocks(){
		return true;
	}
}
