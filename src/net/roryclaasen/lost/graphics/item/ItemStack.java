package net.roryclaasen.lost.graphics.item;

import java.util.ArrayList;
import java.util.List;

import net.gogo98901.log.Log;
import net.roryclaasen.lost.graphics.item.items.ItemTool;

public class ItemStack {
	private int id;
	private List<ItemBase> items = new ArrayList<ItemBase>();

	public ItemStack(ItemBase item) {
		this(item, 1);
	}

	public ItemStack(ItemBase item, int quantity) {
		id = item.getID();
		for (int i = 0; i < quantity; i++) {
			if (!add(item)) {
				Log.warn(item.getName() + " can only hold " + item.stackSize() + " not " + quantity);
			}
		}
	}

	public int getID() {
		return id;
	}

	public String getName() {
		return Items.getItem(id).getName();
	}

	public boolean add(ItemStack item) {
		if (item.getID() == id) {
			if (items.size() + item.getSize() <= getItem().stackSize()) {
				items.add(item.getItem());
				return true;
			}
		}
		return false;
	}

	public boolean add(ItemBase item) {
		if (item.getID() == id) {
			if (items.size() < getItem().stackSize()) {
				items.add(item);
				return true;
			}
		}
		return false;
	}

	public boolean isEmpty() {
		return items.isEmpty();
	}

	public ItemBase getItem() {
		return Items.getItem(id);
	}

	public int getSize() {
		return items.size();
	}

	public void removeFromCount(int number) {
		if (items.size() - number > 0) {
			setSize(items.size() - number);
		}
	}

	public void removeFromArray(int index) {
		items.remove(index);
	}

	public void setSize(int size) {
		items.clear();
		for (int i = 0; i < size; i++) {
			items.add(Items.getItem(id));
		}
	}

	public int getMaxSize() {
		return Items.getItem(id).stackSize();
	}

	public void use() {
		ItemBase item = items.get(items.size() - 1);
		item.use();
		if (item.consumable()) {
			if (((ItemTool) item).isRemoved()) items.remove(items.size() - 1);
			else items.remove(items.size() - 1);
		}
	}

	public boolean isTool() {
		if (items.isEmpty()) return false;
		if (items.get(0) == null) return false;
		return items.get(0) instanceof ItemTool;
	}
}
