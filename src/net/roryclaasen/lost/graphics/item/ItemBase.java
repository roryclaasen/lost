package net.roryclaasen.lost.graphics.item;

import net.roryclaasen.lost.graphics.sprite.Sprite;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;

public abstract class ItemBase {
	private String name = "ItemBase";
	private SpriteSheet sheet;
	protected Sprite sprite;

	private int id;
	private boolean remove = false;
	private int stackSize = 10;

	protected int dammage = 1;

	public ItemBase(SpriteSheet sheet, int id, int stackSize, String name) {
		this.id = id;
		this.stackSize = stackSize;
		this.sheet = sheet;
		if (name != null) this.name = name;
		load();
		Items.items.add(this);
	}

	private void load() {
		if (sheet != null) {
			sprite = sheet.get(id);
		}
	}

	public abstract void use();

	public void remove() {
		remove = true;
	}

	public boolean isRemoved() {
		return remove;
	}

	public abstract boolean consumable();

	public boolean canDestroyBlocks() {
		return false;
	}

	public int stackSize() {
		return stackSize;
	}

	public int getID() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Sprite getSprite() {
		return sprite;
	}

	public int getDammage() {
		return dammage;
	}
}
