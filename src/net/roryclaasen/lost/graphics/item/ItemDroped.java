package net.roryclaasen.lost.graphics.item;

import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.toolbox.TileCords;
import net.roryclaasen.lost.toolbox.list.Fonts;

import org.newdawn.slick.Graphics;

public class ItemDroped {

	@SuppressWarnings("unused")
	private Level level;
	private ItemStack stack;

	private int x, y;
	private TileCords cords;
	private TileCords from;
	private double time;
	private int xOffset, yOffset;

	private int size = 32;

	private boolean removed = false;
	private boolean pickUp = false;
	private boolean drop = false;

	public ItemDroped(Level level, ItemStack stack, TileCords cords) {
		this.level = level;
		this.stack = stack;
		this.setPosition(cords);
	}

	public ItemDroped(Level level, ItemStack stack, TileCords cords, TileCords from) {
		this.level = level;
		this.stack = stack;
		this.setPosition(cords);
		this.setFrom(from);
	}

	public ItemDroped setFrom(TileCords from) {
		this.from = from;
		drop = true;
		size = 10;
		if (from.getY() != cords.getY()) {
			if (from.getY() > cords.getY()) yOffset = 32;
			else yOffset = -32;
		}
		if (from.getX() != cords.getX()) {
			if (from.getX() > cords.getX()) xOffset = 32;
			else xOffset = -32;
		}
		return this;
	}

	public void update() {
		if (drop) {
			if (from != null) {
				time += 2;
				if (size != 32) if (time % 2 == 0) size++;
				if (from.getY() != cords.getY()) {
					if (from.getY() > cords.getY()) yOffset = 32 - (int) time;
					else yOffset = -32 + (int) time;
				}
				if (from.getX() != cords.getX()) {
					if (from.getX() > cords.getX()) xOffset = 32 - (int) time;
					else xOffset = -32 + (int) time;
				}
				if (xOffset == 0 && yOffset == 0 || time > 32) {
					drop = false;
					size = 32;
					time = yOffset = xOffset = 0;
				}
			}
		} else if (pickUp) {
			size -= 2;
			if (size < 25) removed = true;
		} else {
			time += 0.05;
			yOffset = (int) time;
			time %= 3;
		}
	}

	public void pickUp() {
		if (pickUp == false) {
			pickUp = true;
			yOffset = 0;
			time = 0;
			// removed = true;
		}
	}

	public boolean canPickUp() {
		return !pickUp;
	}

	public boolean isRemoved() {
		return removed;
	}

	public int size() {
		return stack.getSize();
	}

	public void render(Graphics g, int xOff, int yOff) {
		Items.getItem(stack.getID()).getSprite().getImage().draw(x * 32 + xOff + ((32 - size) / 2) + xOffset, (y * 32) + yOff + yOffset + ((32 - size) / 2), size, size);
		if (size() > 1) {
			Fonts.drawString(g, Fonts.square24, size(), x * 32 + xOff - ((32 - size) / 2) + 26 + xOffset, y * 32 + yOff - ((32 - size) / 2) + 26 + yOffset, 200);
		}
	}

	public ItemDroped setPosition(TileCords cords) {
		this.cords = cords;
		this.x = cords.getCords().getX();
		this.y = cords.getCords().getY();
		return this;
	}

	public ItemStack getItemStack() {
		return stack;
	}

	public TileCords getTileCords() {
		return cords;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
