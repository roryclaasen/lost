package net.roryclaasen.lost.graphics.item;

import java.util.ArrayList;
import java.util.List;

import net.gogo98901.log.Log;
import net.roryclaasen.lost.graphics.item.items.ItemBackpack;
import net.roryclaasen.lost.graphics.item.items.ItemObject;
import net.roryclaasen.lost.graphics.item.items.ItemTool;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;

public class Items {
	public static List<ItemBase> items = new ArrayList<ItemBase>();

	private static List<Integer> exclude = new ArrayList<Integer>();

	public static ItemBase getItem(int id) {
		for (ItemBase item : items) {
			if (item.getID() == id) return item;
		}
		if (exclude.contains(id)) return null;
		Log.warn("No Item with id of " + id);
		exclude.add(id);
		return null;
	}
	
	public static final ItemBase stick = new ItemObject(SpriteSheet.items, 0, 5, "Stick");
	
	public static final ItemBase backpack0 = new ItemBackpack(SpriteSheet.items, 16, 1, "BackPack");
	public static final ItemBase backpack1 = new ItemBackpack(SpriteSheet.items, 17, 1, "Lucky BackPack");
	public static final ItemBase backpack2 = new ItemBackpack(SpriteSheet.items, 18, 1, "Goo BackPack");
	public static final ItemBase backpack3 = new ItemBackpack(SpriteSheet.items, 19, 1, "Golden BackPack");
	public static final ItemBase backpack4 = new ItemBackpack(SpriteSheet.items, 20, 1, "Fur BackPack");
	public static final ItemBase backpack5 = new ItemBackpack(SpriteSheet.items, 21, 1, "Bandit BackPack");
	public static final ItemBase backpack6 = new ItemBackpack(SpriteSheet.items, 22, 1, "Expedition BackPack");
	public static final ItemBase backpack7 = new ItemBackpack(SpriteSheet.items, 23, 1, "Wolf BackPack");
	public static final ItemBase backpack8 = new ItemBackpack(SpriteSheet.items, 24, 1, "Devil BackPack");

	public static final ItemBase axe = new ItemTool(SpriteSheet.items, 32, 1, "Axe");
}
