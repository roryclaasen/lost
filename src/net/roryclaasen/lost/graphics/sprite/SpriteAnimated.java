package net.roryclaasen.lost.graphics.sprite;

import java.util.ArrayList;
import java.util.List;

public class SpriteAnimated {
	private int time = 0;
	private int interval = 30;
	private int frame;

	private List<Sprite> sprites = new ArrayList<Sprite>();
	private Sprite sprite;

	public SpriteAnimated(SpriteSheet sheet, int[] ids) {
		for (int id : ids) {
			sprites.add(sheet.get(id));
		}
		setSprite(0);
	}

	public void update() {
		time++;
		if (time % interval == 0) {
			frame++;
			if (frame >= sprites.size()) {
				frame = 0;
				time = 0;
			}
			setSprite(frame);
		}
	}

	public void setSprite(int frame) {
		sprite = sprites.get(this.frame = frame);
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public Sprite getSprite() {
		return sprite;
	}
}
