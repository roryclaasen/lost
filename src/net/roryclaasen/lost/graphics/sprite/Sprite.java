package net.roryclaasen.lost.graphics.sprite;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Sprite {
	public static final Sprite shaddow64 = new Sprite("textures/shaddow.64.png");
	public static final Sprite shaddow32 = new Sprite("textures/shaddow.32.png");
	public static final Sprite shaddow16 = new Sprite("textures/shaddow.16.png");

	private Image image;
	private int id;
	private int width, height;

	public Sprite(String imagePath, int id) {
		this.image = load("assets/" + imagePath);
		this.id = id;
		if (image != null) {
			width = image.getWidth();
			height = image.getHeight();
		}
	}

	public Sprite(String imagePath) {
		this(imagePath, -1);
	}

	public Sprite(Image image, int id) {
		this.image = image;
		this.id = id;
		if (image != null) {
			width = image.getWidth();
			height = image.getHeight();
		}
	}

	public Sprite(Image image) {
		this(image, -1);
	}

	private Image load(String imagePath) {
		try {
			return new Image(imagePath);
		} catch (SlickException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Sprite getSubSprite(int x, int y, int width, int height) {
		if (x < 0 || y < 0 || x + width > this.width || y + height > this.height) {
			try {
				throw new Exception("Out of bounds");
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return new Sprite(image.getSubImage(x, y, width, height));
	}

	public Image getImage() {
		return image;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public boolean hasId() {
		return id >= 0;
	}

	public int getID() {
		return id;
	}
}
