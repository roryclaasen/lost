package net.roryclaasen.lost.graphics.sprite;

import net.roryclaasen.lost.graphics.item.ItemBase;
import net.roryclaasen.lost.level.entity.Mob;
import net.roryclaasen.lost.toolbox.Direction;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class ItemSwingAnimation {

	private Mob mob;
	private Image image;

	private int tick;

	private float startAngle, currentAngle, endAngle, defAngle = 45F;
	private float speed = 5F;

	private boolean running;

	private int xoff, yoff;

	public ItemSwingAnimation(Mob mob) {
		this.mob = mob;
	}

	public ItemSwingAnimation setImage(ItemBase item) {
		return setImage(item.getSprite());
	}

	public ItemSwingAnimation setImage(Sprite sprite) {
		return setImage(sprite.getImage());
	}

	public ItemSwingAnimation setImage(Image image) {
		this.image = image.copy().getFlippedCopy(true, true);
		this.image.setFilter(Image.FILTER_NEAREST);
		return this;
	}

	public void update() {
		if (running) {
			if (tick == 0) {
				currentAngle -= speed;
				if (currentAngle < endAngle) {
					running = false;
					if (image != null) image.setRotation(0);
				}
			}
			tick++;
			tick %= 2;
			if (image != null) image.setRotation(currentAngle);
		}
	}

	public void render(Graphics g, int x_off, int y_off) {
		if (running && image != null) {
			image.draw(x_off + mob.getX() + 16 + xoff, y_off + mob.getY() + 16 + yoff, -32, -32);
		}
	}

	public void doAnimation(Direction dir) {
		if (!running) {
			if (dir == Direction.NORTH) {
				startAngle = defAngle;
				xoff = 0;
				yoff = 0;
			}
			if (dir == Direction.EAST) {
				startAngle = defAngle + 90F;
				xoff = -32;
				yoff = 0;
			}
			if (dir == Direction.SOUTH) {
				startAngle = defAngle + 180F;
				xoff = -32;
				yoff = -32;
			}
			if (dir == Direction.WEST) {
				startAngle = defAngle + 270F;
				xoff = 0;
				yoff = -32;
			}
			endAngle = startAngle - 45F;
			currentAngle = startAngle;
			running = true;
		}
	}
}