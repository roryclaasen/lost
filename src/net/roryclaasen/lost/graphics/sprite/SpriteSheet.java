package net.roryclaasen.lost.graphics.sprite;

import java.util.ArrayList;
import java.util.List;

import net.gogo98901.log.Log;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class SpriteSheet {

	public static final SpriteSheet sheet1 = new SpriteSheet("Sheet.png");
	public static final SpriteSheet items = new SpriteSheet("item.png");

	public static final SpriteSheet player = new SpriteSheet("player.png");
	public static final SpriteSheet dummy = new SpriteSheet("dummy.png");
	public static final SpriteSheet chaser = new SpriteSheet("chaser.png");
	public static final SpriteSheet guard = new SpriteSheet("guard.png");

	private Image sheet;
	private String name;

	private List<Sprite> sprites = new ArrayList<Sprite>();

	public SpriteSheet(String name) {
		this.name = name;
		load();
	}

	private void load() {
		try {
			sheet = new Image("assets/textures/" + name);

			for (int y = 0; y < sheet.getHeight() / 32; y++) {
				for (int x = 0; x < sheet.getWidth() / 32; x++) {
					sprites.add(new Sprite(sheet.getSubImage(x * 32, y * 32, 32, 32), y * sheet.getHeight() + x));
				}
			}
			Log.info("Loaded Spritesheet [" + name + "]");
		} catch (SlickException e) {
			Log.info("Did not load Spritesheet [" + name + "]");
		}
	}

	public Sprite get(int id) {
		return sprites.get(id);
	}

	public Image getSheet() {
		return sheet;
	}
}
