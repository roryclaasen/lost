package net.roryclaasen.lost.graphics.tile;

import java.util.ArrayList;
import java.util.List;

import net.gogo98901.log.Log;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.graphics.tile.tiles.TileAir;
import net.roryclaasen.lost.graphics.tile.tiles.TileBreakable;
import net.roryclaasen.lost.graphics.tile.tiles.TileGround;
import net.roryclaasen.lost.graphics.tile.tiles.TileVoid;
import net.roryclaasen.lost.graphics.tile.tiles.TileWall;

public class Tiles {

	public static List<TileBase> tiles = new ArrayList<TileBase>();

	private static List<Integer> exclude = new ArrayList<Integer>();

	public static TileBase getTile(int id) {
		for (TileBase tile : tiles) {
			if (tile.getID() == id) return tile;
		}
		if (exclude.contains(id)) return voidTile;
		Log.warn("No Tile with id of " + id + ", using void texture");
		exclude.add(id);
		return voidTile;
	}

	public static final TileBase voidTile = new TileVoid(-1);

	public static final TileBase grass = new TileGround(SpriteSheet.sheet1, 0, "Grass");
	public static final TileBase stone = new TileGround(SpriteSheet.sheet1, new int[]{6, 7, 8, 22, 23, 24, 38, 39, 40}, "Stone");
	public static final TileBase stoneWall = new TileWall(SpriteSheet.sheet1, new int[]{17, 18, 19, 20, 21, 33, 34, 35, 36, 37, 49, 50, 51, 65, 66, 67, 68, 69, 81, 83, 84, 85, 97, 98, 99}, "Stone Wall");

	public static final TileBase darkness = new TileAir(SpriteSheet.sheet1, new int[]{16, 32, 48, 64, 113, 114, 115, 116, 117, 129, 131, 132, 133, 145, 146, 147, 148, 149}, "Darkness");

	public static final TileBase create = new TileBreakable(SpriteSheet.sheet1, new int[]{9, 10}, "Create", 5);
}