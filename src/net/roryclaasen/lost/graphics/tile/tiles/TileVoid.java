package net.roryclaasen.lost.graphics.tile.tiles;


import java.awt.image.BufferedImage;

import net.roryclaasen.lost.graphics.tile.TileBase;

public class TileVoid extends TileBase {

	public TileVoid(int id) {
		super(null, id, "Void");
		BufferedImage image = new BufferedImage(2, 2, BufferedImage.TYPE_INT_RGB);
		image.setRGB(0, 0, (255 << 16) | (0 << 8) | 255);
		image.setRGB(0, 1, (0 << 16) | (0 << 8) | 0);
		image.setRGB(1, 0, (0 << 16) | (0 << 8) | 0);
		image.setRGB(1, 1, (255 << 16) | (0 << 8) | 255);
		// TODO Set texture
	}

	public boolean isSolid() {
		return false;
	}
}
