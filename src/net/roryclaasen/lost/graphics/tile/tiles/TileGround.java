package net.roryclaasen.lost.graphics.tile.tiles;

import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.graphics.tile.TileBase;

public class TileGround extends TileBase {

	public TileGround(SpriteSheet sheet, int id, String name) {
		super(sheet, id, name);
	}

	public TileGround(SpriteSheet sheet, int[] ids, String name) {
		super(sheet, ids[0], name);
		for (int i = 1; i < ids.length; i++) {
			new TileGround(sheet, ids[i], name);
		}
	}

	public boolean isSolid() {
		return false;
	}
}
