package net.roryclaasen.lost.graphics.tile.tiles;

import net.roryclaasen.lost.event.Block.BlockListener;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.graphics.tile.TileBase;

public class TileBreakable extends TileBase {

	private int health;

	private BlockListener listener;

	public TileBreakable(SpriteSheet sheet, int id, String name, int health) {
		super(sheet, id, name);
		this.health = health;
	}

	public TileBreakable(SpriteSheet sheet, int[] ids, String name, int health) {
		super(sheet, ids[0], name);
		this.health = health;
		for (int i = 1; i < ids.length; i++) {
			new TileBreakable(sheet, ids[i], name, health);
		}
	}

	public TileBreakable addBlockListener(BlockListener listener) {
		this.listener = listener;
		return this;
	}

	public BlockListener getListener() {
		return listener;
	}

	public boolean isSolid() {
		return true;
	}

	public int getHealth() {
		return health;
	}
}