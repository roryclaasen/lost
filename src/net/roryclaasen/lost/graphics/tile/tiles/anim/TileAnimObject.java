package net.roryclaasen.lost.graphics.tile.tiles.anim;

import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.graphics.tile.TileBaseAnimated;

public class TileAnimObject extends TileBaseAnimated {

	public TileAnimObject(SpriteSheet sheet, int id, int[] ids, String name) {
		super(sheet, id, ids, name);
	}
	
	public boolean isSolid(){
		return true;
	}
}
