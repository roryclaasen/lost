package net.roryclaasen.lost.graphics.tile;

import net.roryclaasen.lost.graphics.sprite.Sprite;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.toolbox.Arguments;
import net.roryclaasen.lost.toolbox.list.Colors;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class TileBase {

	private String name = "TileBase";
	private SpriteSheet sheet;
	protected Sprite sprite;
	private int id;

	public TileBase(SpriteSheet sheet, int id, String name) {
		this.sheet = sheet;
		this.id = id;
		if (name != null) this.name = name;
		load();
		Tiles.tiles.add(this);
	}

	private void load() {
		if (sheet != null) {
			sprite = sheet.get(id);
		}
	}

	public void render(Graphics g, int x, int y) {
		if (sprite != null) {
			sprite.getImage().draw(x, y, 32, 32);
			if (Arguments.showBounds()) {
				Color c = g.getColor();
				g.setColor(Colors.boxTile);
				g.drawRect(x, y, 32, 32);
				g.setColor(c);
			}
		}
	}

	public boolean isSolid() {
		return false;
	}

	public String getName() {
		return name;
	}

	public boolean showName() {
		return false;
	}

	public int getID() {
		return id;
	}
}
