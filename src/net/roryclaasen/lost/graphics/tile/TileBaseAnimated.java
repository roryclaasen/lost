package net.roryclaasen.lost.graphics.tile;

import java.awt.Graphics;

import net.roryclaasen.lost.graphics.sprite.SpriteAnimated;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;

public class TileBaseAnimated extends TileBase {

	protected SpriteAnimated anim;

	/**
	 * Id is for the game to know what tile to show the tile
	 * 
	 * @param sheet
	 * @param id
	 * @param ids
	 * @param name
	 */
	public TileBaseAnimated(SpriteSheet sheet, int id, int[] ids, String name) {
		super(sheet, id, name + " Animated");
		anim = new SpriteAnimated(sheet, ids);
		anim.setInterval(30);
	}

	public TileBaseAnimated setInterval(int interval) {
		if (anim != null) anim.setInterval(interval);
		return this;
	}

	public void render(Graphics g, int x, int y) {
		if (sprite != null) {
			sprite.getImage().draw(x, y, 32, 32);
		}
	}

	public void update() {
		if (anim != null) {
			anim.update();
			sprite = anim.getSprite();
		}
	}
}
