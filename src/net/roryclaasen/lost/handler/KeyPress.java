package net.roryclaasen.lost.handler;

import net.roryclaasen.lost.event.Key.KeyEvent;
import net.roryclaasen.lost.event.Key.KeyEventListener;

public class KeyPress {
	private KeyEventListener listener;
	private int code = -1;
	private boolean output;

	public KeyPress(int code) {
		this.code = code;
	}

	public void update() {
		if (code != -1) {
			if (HandlerKeyboard.get(code)) {
				output = true;
				callPress();
			} else {
				if (output) {
					callRelease();
					output = false;
				}
			}
		}
	}

	public void callPress() {
		if (listener != null) listener.keyPress(new KeyEvent(this));
	}

	public void callRelease() {
		if (listener != null) listener.keyRelease(new KeyEvent(this));
	}

	public void addListener(KeyEventListener listener) {
		this.listener = listener;
	}

	public void removeListener() {
		this.listener = null;
	}
}
