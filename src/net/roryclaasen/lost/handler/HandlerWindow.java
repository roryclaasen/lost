package net.roryclaasen.lost.handler;

import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowListener;

import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.state.States;

public class HandlerWindow implements WindowListener, WindowFocusListener {

	private GameThread game;

	public HandlerWindow(GameThread game) {
		this.game = game;
	}

	@Override
	public void windowGainedFocus(WindowEvent arg0) {}

	@Override
	public void windowLostFocus(WindowEvent arg0) {
		if (game.canvas.states.is(States.State.GAME)) {
			game.canvas.states.getGameState().pause();
		}
	}

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}

	@Override
	public void windowClosing(WindowEvent arg0) {
		game.getContainer().exit();
		game.closeFunction();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}
}