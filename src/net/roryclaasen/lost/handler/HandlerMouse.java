package net.roryclaasen.lost.handler;

import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.state.States;
import net.roryclaasen.lost.toolbox.Direction;
import net.roryclaasen.lost.toolbox.Util;
import net.roryclaasen.lost.window.DisplayWindow;

import org.newdawn.slick.Input;
import org.newdawn.slick.MouseListener;

public class HandlerMouse implements MouseListener {

	public static final int BTN_LEFT = 0;
	public static final int BTN_WHEEL = 2;
	public static final int BTN_RIGHT = 1;
	public static final int BTN_MOUSE3 = 3;
	public static final int BTN_MOUSE4 = 4;

	private GameThread game;
	private int time;
	private static int x, y;
	private static int button = -1;
	private static int wheel;

	private static Direction position = Direction.CENTER;

	public HandlerMouse(GameThread game) {
		this.game = game;
	}

	@Override
	public void setInput(Input input) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean isAcceptingInput() {
		return true;
	}

	@Override
	public void inputEnded() {
		// TODO Auto-generated method stub
	}

	@Override
	public void inputStarted() {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseWheelMoved(int change) {
		wheel += change;
		if (game.canvas.states.is(States.State.GAME)) {
			if (!game.canvas.states.getGameState().isPaused()) {
				int steps = Util.abs(change);
				if (change < 0) game.canvas.states.getGameState().getUserInterface().getInv().up(-steps);
				else game.canvas.states.getGameState().getUserInterface().getInv().down(steps);
			}
		}
	}

	@Override
	public void mouseClicked(int button, int x, int y, int clickCount) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mousePressed(int button, int x, int y) {
		HandlerMouse.button = button;
	}

	@Override
	public void mouseReleased(int button, int x, int y) {
		HandlerMouse.button = -1;
	}

	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		x = newx;
		y = newy;
	}

	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
		x = newx;
		y = newy;
	}

	public void update() {
		if (game.canvas.states.is(States.State.GAME)) {
			if (!game.canvas.states.getGameState().isPaused()) {
				if (button == BTN_LEFT) {
					if (time == 0) {
						if (game.canvas.states.getGameState().getLevel().getPlayer() != null) {
							game.canvas.states.getGameState().getLevel().getPlayer().use(getPositionOnScreen());
						}
					}
					time++;
					time %= game.canvas.states.getGameState().getLevel().getPlayer().getAttackSpeed();
				}
			}
		}
	}

	public static int getX() {
		return x;
	}

	public static int getY() {
		return y;
	}

	public static int getButton() {
		return button;
	}

	public static int getWheel() {
		return wheel;
	}

	/**
	 * Please note that this only detects the middle 600x600 pixels due to bug
	 * 
	 * @see <a href="https://github.com/GOGO98901/Lost/issues/10">Github Issue</a>
	 * @return Direction position
	 */
	public static Direction getPositionOnScreen() {
		try {
			int width = /* DisplayWindow.getWidth() */600, height = /* DisplayWindow.getHeight() */600;
			int mX = x - ((DisplayWindow.getWidth() - width) / 2), mY = y - ((DisplayWindow.getHeight() - height) / 2);
			double gradient;
			double mGradient = -(mX) / (mY);
			if (mX < width / 2) {
				if (mY < height / 2) {
					gradient = (-(height / 2) / (width / 2));
					if (mGradient > gradient) position = Direction.WEST;
					else position = Direction.NORTH;
				} else {
					gradient = ((height / 2) / (width / 2));
					mY = height - y;
					mGradient = (mX) / (mY);
					if (mGradient >= gradient) position = Direction.SOUTH;
					else position = Direction.WEST;
				}
			} else {
				if (mY < height / 2) {
					gradient = ((height / 2) / (width / 2));
					mY = height - y;
					mGradient = (mX) / (mY);
					if (mGradient >= gradient) position = Direction.EAST;
					else position = Direction.NORTH;
				} else {
					gradient = (-(height / 2) / (width / 2));
					if (mGradient > gradient) position = Direction.SOUTH;
					else position = Direction.EAST;
				}
			}
		} catch (Exception e) {
			// Caused by "Can't be / by zero."
			position = Direction.CENTER;
		}
		return position;
	}

}
