package net.roryclaasen.lost.handler;

import net.gogo98901.log.Log;
import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.window.DisplayWindow;

import org.newdawn.slick.GameContainer;

public class Handler {
	public HandlerKeyboard keyboard;
	public HandlerMouse mouse;
	public HandlerWindow window;
	

	public Handler(GameThread game) {
		keyboard = new HandlerKeyboard();
		mouse = new HandlerMouse(game);
		window = new HandlerWindow(game);
	}

	public void addAll(GameContainer container) {
		container.getInput().addMouseListener(mouse);
		container.getInput().addKeyListener(keyboard);

		DisplayWindow.getFrame().addWindowListener(window);
		DisplayWindow.getFrame().addWindowFocusListener(window);
		
		Log.info("Handlers... ADDED");
	}
}
