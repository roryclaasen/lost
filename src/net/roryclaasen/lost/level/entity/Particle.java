package net.roryclaasen.lost.level.entity;

import net.roryclaasen.lost.graphics.sprite.Sprite;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.toolbox.list.Colors;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Particle extends Entity {

	public final static int SMALL = 1, MEDIUM = 3, LARGE = 6;

	private int size = SMALL;
	private Color color = Color.gray;
	private Sprite sprite;
	private int life, time = 0;

	private boolean darkOverlay = true;

	protected double xx, yy, zz;
	protected double xa, ya, za;

	public Particle(Level level, double x, double y, int life, int size, Sprite texture) {
		this(level, x, y, life, size, 1, texture);
	}

	public Particle(Level level, double x, double y, int life, int size) {
		this(level, x, y, life, size, null);
	}

	public Particle(Level level, double x, double y, int life, int size, int amount, Sprite texture) {
		super(level);
		this.x = x;
		this.y = y;
		this.size = size + (random.nextInt(3 * 2) - 3);
		if (size <= 0) size = 1;

		this.xx = x;
		this.yy = y;
		this.life = life + (random.nextInt(life * 2) - life);

		this.xa = random.nextGaussian();
		this.ya = random.nextGaussian();
		this.zz = random.nextFloat() + 2.0;

		if (texture != null) genSprite(texture);

		for (int i = 1; i < amount; i++) {
			level.add(new Particle(level, x, y, life, size, texture));
		}
	}

	public Particle(Level level, double x, double y, int life, int size, int amount) {
		this(level, x, y, life, size, amount, null);
	}

	@Override
	public void render(Graphics g, int xOffset, int yOffset) {
		int renderX = (int) xx - 1;
		int renderY = (int) yy - (int) zz - 2;
		g.setColor(color);
		g.fillRect(xOffset + renderX - (size / 2), yOffset + renderY - (size / 2), size, size);
		if (sprite != null) {
			sprite.getImage().draw(xOffset + renderX - (size / 2), yOffset + renderY - (size / 2), size, size);
		}
		if (darkOverlay) {
			g.setColor(Colors.setOpacity(Color.black, 150));
			g.drawRect(xOffset + renderX - (size / 2), yOffset + renderY - (size / 2), size, size);
		}
	}

	@Override
	public void update() {
		time++;
		// time *= delta;
		if (time > 7400) time = 0;
		if (time > life) remove();
		za -= 0.1;

		if (zz < 0) {
			zz = 0;
			za *= -1;
			xa *= 0.4;
			ya *= 0.4;
		}
		move(xx + xa, (yy + ya) + (zz + za));
	}

	public boolean collision(double x, double y) {
		boolean solid = false;
		for (int c = 0; c < 4; c++) {
			double xt = (x - c % 2 * 32) / 32;
			double yt = (y - c / 2 * 32) / 32;
			int xi = (int) Math.ceil(xt);
			int yi = (int) Math.ceil(yt);
			if (c % 2 == 0) xi = (int) Math.floor(xt);
			if (c / 2 == 0) yi = (int) Math.floor(yt);
			if (level.getTile(xi, yi).isSolid()) solid = true;
		}
		return solid;
	}

	private void move(double x, double y) {
		if (collision(x, y)) {
			xa *= -0.4;
			ya *= -0.4;
			za *= -0.4;
		}
		this.xx += xa;
		this.yy += ya;
		this.zz += za;
	}

	public Particle setSize(int size) {
		this.size = size;
		return this;
	}

	public Particle setColor(int color) {
		this.color = new Color(color);
		return this;
	}

	public Particle setColor(Color color) {
		this.color = color;
		return this;
	}

	public Particle genSprite(Sprite sprite) {
		int ranX = random.nextInt(sprite.getWidth() - size);
		int ranY = random.nextInt(sprite.getWidth() - size);
		this.sprite = sprite.getSubSprite(ranX, ranY, size, size);
		return this;
	}

	@Override
	public String getName() {
		return "Particle";
	}
}
