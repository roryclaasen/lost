package net.roryclaasen.lost.level.entity;


import net.roryclaasen.lost.event.Block.BlockEvent;
import net.roryclaasen.lost.event.Block.BlockListener;
import net.roryclaasen.lost.toolbox.TileCords;

import org.newdawn.slick.Graphics;

public class Obstruction extends Entity {

	private int health;

	private BlockListener listener;

	public Obstruction(int x, int y, int health) {
		super(null);
		this.x = x;
		this.y = y;
		this.health = health;
	}

	public TileCords getPosition() {
		return new TileCords((int) x, (int) y);
	}

	public void doDamage(int damage) {
		health -= damage;
		callBlockHit();
		if (destroyed()) callBlockDestroyed();
	}

	public int getHealth() {
		return health;
	}

	public boolean isRemoved() {
		return destroyed();
	}

	public boolean destroyed() {
		return health <= 0;
	}

	public Obstruction addBlockListener(BlockListener listener) {
		this.listener = listener;
		return this;
	}

	public void callBlockHit() {
		if (listener != null) listener.blockHit(new BlockEvent(this));
	}

	public void callBlockDestroyed() {
		if (listener != null) listener.blockDestroyed(new BlockEvent(this));
	}

	@Override
	@Deprecated
	public void update() {
	}

	@Override
	@Deprecated
	public void render(Graphics g, int xOffset, int yOffset) {
	}

	@Override
	public String getName() {
		return "Obstruction";
	}
}