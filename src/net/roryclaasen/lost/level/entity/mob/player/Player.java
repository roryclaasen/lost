package net.roryclaasen.lost.level.entity.mob.player;

import java.util.List;

import net.roryclaasen.lost.graphics.item.ItemBase;
import net.roryclaasen.lost.graphics.item.ItemStack;
import net.roryclaasen.lost.graphics.sprite.ItemSwingAnimation;
import net.roryclaasen.lost.graphics.sprite.Sprite;
import net.roryclaasen.lost.graphics.sprite.SpriteAnimated;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.handler.HandlerKeyboard;
import net.roryclaasen.lost.handler.HandlerMouse;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.level.entity.Mob;
import net.roryclaasen.lost.options.Options;
import net.roryclaasen.lost.toolbox.Arguments;
import net.roryclaasen.lost.toolbox.Direction;
import net.roryclaasen.lost.toolbox.list.Colors;
import net.roryclaasen.lost.window.DisplayWindow;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Player extends Mob {

	private SpriteAnimated animDown = new SpriteAnimated(SpriteSheet.player, new int[]{0, 1, 2});
	private SpriteAnimated animLeft = new SpriteAnimated(SpriteSheet.player, new int[]{3, 4, 5});
	private SpriteAnimated animRight = new SpriteAnimated(SpriteSheet.player, new int[]{6, 7, 8});
	private SpriteAnimated animUp = new SpriteAnimated(SpriteSheet.player, new int[]{9, 10, 11});

	public Inventory inv;
	private ItemSwingAnimation item;

	private Direction keyDirection = Direction.CENTER;

	private int shaddowSize = Options.playerShaddowSize.get();

	public Player(Level level) {
		super(level, SpriteSheet.player.get(1));
		anim = animDown;
		boxColor = Colors.boxPlayer;
		healthMax = health = 50;
		inv = new Inventory(level, this);
		item = new ItemSwingAnimation(this);
		shaddowSize = Options.playerShaddowSize.get();
	}

	@Override
	public void update() {
		time++;
		time %= 600;
		inv.update();
		if (canRoam()) {
			int xa = 0, ya = 0;
			if (!moving) {
				pickUp();
				anim.setSprite(1);
				anim.setInterval(7);

				if (keyDirection == Direction.CENTER && HandlerKeyboard.w) keyDirection = Direction.NORTH;
				else if (keyDirection == Direction.NORTH && !HandlerKeyboard.w) keyDirection = Direction.CENTER;
				if (keyDirection == Direction.CENTER && HandlerKeyboard.d) keyDirection = Direction.EAST;
				else if (keyDirection == Direction.EAST && !HandlerKeyboard.d) keyDirection = Direction.CENTER;
				if (keyDirection == Direction.CENTER && HandlerKeyboard.s) keyDirection = Direction.SOUTH;
				else if (keyDirection == Direction.SOUTH && !HandlerKeyboard.s) keyDirection = Direction.CENTER;
				if (keyDirection == Direction.CENTER && HandlerKeyboard.a) keyDirection = Direction.WEST;
				else if (keyDirection == Direction.WEST && !HandlerKeyboard.a) keyDirection = Direction.CENTER;
				if (keyDirection != Direction.CENTER) {
					if (keyDirection == Direction.NORTH) ya -= speed;
					if (keyDirection == Direction.EAST) xa += speed;
					if (keyDirection == Direction.SOUTH) ya += speed;
					if (keyDirection == Direction.WEST) xa -= speed;
				}
				// if (!(HandlerKeyboard.w || HandlerKeyboard.d || HandlerKeyboard.s || HandlerKeyboard.a)) keyDirection = Direction.CENTER;
				move(xa, ya);
			}
			if (HandlerKeyboard.space) {
				if (attackTime == 0) {
					Direction dir = getDirection();
					if (Options.allowMouseAttacking.get()) dir = HandlerMouse.getPositionOnScreen();

					level.attack(this, dir);
					level.attack(this, getPosition());

					if (inv.canGetCurrentItem()) {
						item.setImage(inv.getCurrentItem());
					}
					item.doAnimation(dir);
				}
				attackTime++;
				attackTime %= attackSpeed;
			} else attackTime = 0;
		}
		if (getDirection() == Direction.CENTER) anim = animDown;
		if (getDirection() == Direction.SOUTH) anim = animDown;
		if (getDirection() == Direction.NORTH) anim = animUp;
		if (getDirection() == Direction.WEST) anim = animLeft;
		if (getDirection() == Direction.EAST) anim = animRight;
		if (anim != null) sprite = anim.getSprite();
		
		item.update();
	}

	public void renderExtra(Graphics g, int x_off, int y_off) {
		item.render(g, x_off, y_off);
	}

	public void renderShaddow(Graphics g, int x_off, int y_off) {
		if (shaddowSize > 100) {
			int x0 = x_off + getX() - (shaddowSize / 2) + 16;
			int x1 = x_off + getX() + (shaddowSize / 2) + 16;
			int y0 = y_off + getY() - (shaddowSize / 2) + 16;
			int y1 = y_off + getY() + (shaddowSize / 2) + 16;
			g.setColor(Color.black);
			g.fillRect(0, 0, x0, DisplayWindow.getHeight());
			g.fillRect(x1, 0, DisplayWindow.getWidth() - x1, DisplayWindow.getHeight());
			g.fillRect(0, 0, DisplayWindow.getWidth(), y0);
			g.fillRect(0, y1, DisplayWindow.getWidth(), DisplayWindow.getHeight() - y1);
			Sprite.shaddow32.getImage().draw(x0, y0, shaddowSize, shaddowSize);
			if (Arguments.showBounds()) {
				Color c = g.getColor();
				g.setColor(Colors.boxPlayer);
				g.drawRect(x0, y0, shaddowSize, shaddowSize);
				g.setColor(c);
			}
		}
	}

	public void attack(Direction dir) {
		level.attack(this, dir);
	}

	public void use(Direction dir) {
		if (inv.use()) {
			if (inv.canGetCurrentItem()) {
				item.setImage(inv.getCurrentItem());
			}
			item.doAnimation(dir);
			attack(dir);
		}
	}

	public void setShaddowSize(int size) {
		this.shaddowSize = size;
	}

	@Override
	public String getName() {
		return "Player";
	}

	public boolean pickUp(ItemStack itemStack) {
		return inv.pickUp(itemStack);
	}

	public void setInventory(List<ItemStack> loot) {
		this.loot.clear();
		for (ItemStack stack : loot) {
			pickUp(stack);
		}
	}

	public ItemBase getHeldItem() {
		return inv.getCurrentItem();
	}
}
