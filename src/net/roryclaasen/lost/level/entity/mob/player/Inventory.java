package net.roryclaasen.lost.level.entity.mob.player;

import net.roryclaasen.lost.graphics.item.ItemBase;
import net.roryclaasen.lost.graphics.item.ItemDroped;
import net.roryclaasen.lost.graphics.item.ItemStack;
import net.roryclaasen.lost.graphics.item.items.ItemBackpack;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.toolbox.Direction;
import net.roryclaasen.lost.toolbox.TileCords;

public class Inventory {
	public static final int SLOTS_MAX = 10;
	public static final int SLOTS_NO_BACKPACK = 5;

	private int selected = 0;

	private ItemStack[] stacks = new ItemStack[SLOTS_MAX];

	private Player player;
	private Level level;

	public Inventory(Level level, Player player) {
		this.level = level;
		this.player = player;
	}

	public void update() {

	}

	public void drop(int slot) {
		ItemStack old = null;
		if (stacks[slot] != null) {
			old = stacks[slot];
			TileCords cord = player.getPosition();
			Direction dir = player.getDirection();
			int xOff = 0, yOff = 0;
			if (dir == Direction.NORTH) yOff = -1;
			if (dir == Direction.SOUTH) yOff = 1;
			if (dir == Direction.WEST) xOff = -1;
			if (dir == Direction.EAST) xOff = 1;
			TileCords dropCord = new TileCords(cord.getX() + xOff, cord.getY() + yOff);
			if (level.getTile(dropCord).isSolid()) dropCord = player.getPosition();
			level.add(new ItemDroped(level, stacks[slot], dropCord, player.getPosition()));
			stacks[slot] = null;
			if (old.getItem() instanceof ItemBackpack && !hasBackpack()) {
				for (int i = SLOTS_NO_BACKPACK - 1; i < SLOTS_MAX; i++) {
					drop(i);
				}
			}
		}
	}

	public boolean pickUp(ItemStack itemStack) {
		int slotsToCheck = SLOTS_NO_BACKPACK - 1;
		if (hasBackpack()) slotsToCheck = SLOTS_MAX - 1;
		for (int i = 0; i < slotsToCheck; i++) {
			ItemStack stack = stacks[i];
			if (stack == null) {
				stacks[i] = itemStack;
				return true;
			}
			if (itemStack != null) {
				if (stack.getID() == itemStack.getID()) {
					while (stack.getSize() < stack.getMaxSize()) {
						stack.add(itemStack.getItem());
						itemStack.removeFromArray(0);
						if (itemStack.isEmpty()) return true;
					}
				}
			}
		}
		return false;
	}

	public void use(int slot) {
		if (stacks[slot] != null) {
			stacks[slot].use();
			if (stacks[slot].getSize() == 0) stacks[slot] = null;
		}
	}

	public boolean use() {
		if (stacks[selected] != null) {
			stacks[selected].use();
			if (stacks[selected].isTool()) {
				stacks[selected].getItem().use();
			} else if (stacks[selected].getSize() == 0) stacks[selected] = null;
			return true;
		}
		return false;
	}

	public boolean hasBackpack() {
		for (ItemStack stack : stacks) {
			if (stack != null) if (stack.getItem() instanceof ItemBackpack) return true;
		}
		return false;
	}

	public ItemStack[] getStacks() {
		return stacks;
	}

	public int getSelected() {
		return selected;
	}

	public void setSelected(int sel) {
		selected = sel;
	}

	public boolean canGetCurrentItem() {
		if (getCurrentItem() == null) return false;
		return true;
	}

	public ItemBase getCurrentItem() {
		if (stacks[selected] == null) return null;
		return stacks[selected].getItem();
	}
}
