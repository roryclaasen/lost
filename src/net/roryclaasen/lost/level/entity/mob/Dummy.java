package net.roryclaasen.lost.level.entity.mob;

import net.roryclaasen.lost.graphics.sprite.SpriteAnimated;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.level.entity.Mob;
import net.roryclaasen.lost.toolbox.Direction;

public class Dummy extends Mob {

	private int moveTime = 60;
	private SpriteAnimated animDown = new SpriteAnimated(SpriteSheet.dummy, new int[]{0, 1, 2});
	private SpriteAnimated animLeft = new SpriteAnimated(SpriteSheet.dummy, new int[]{3, 4, 5});
	private SpriteAnimated animRight = new SpriteAnimated(SpriteSheet.dummy, new int[]{6, 7, 8});
	private SpriteAnimated animUp = new SpriteAnimated(SpriteSheet.dummy, new int[]{9, 10, 11});

	public Dummy(Level level) {
		super(level, SpriteSheet.dummy.get(1));
		anim = animDown;
		moveTime += random.nextInt(60) + 20 + (random.nextInt(20) - 10);
		time = random.nextInt(60);
		healthMax = 10;
		health = 10;
	}

	@Override
	public void update() {
		time++;
		if (canRoam()) {
			if (!moving) {
				anim.setInterval(7);
				anim.setSprite(1);
				if (time % moveTime == 0) {
					int dir = random.nextInt(4);
					if (dir == 0) move(0, -1);
					if (dir == 1) move(1, 0);
					if (dir == 2) move(0, 1);
					if (dir == 3) move(-1, 0);
				}
			}
			if (getDirection() == Direction.SOUTH) anim = animDown;
			if (getDirection() == Direction.NORTH) anim = animUp;
			if (getDirection() == Direction.WEST) anim = animLeft;
			if (getDirection() == Direction.EAST) anim = animRight;
			if (anim != null) sprite = anim.getSprite();
		}
	}

	@Override
	public String getName() {
		return "Dummy";
	}
}
