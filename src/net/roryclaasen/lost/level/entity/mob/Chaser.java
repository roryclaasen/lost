package net.roryclaasen.lost.level.entity.mob;

import java.util.List;

import net.roryclaasen.lost.graphics.sprite.SpriteAnimated;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.level.Node;
import net.roryclaasen.lost.level.entity.Mob;
import net.roryclaasen.lost.toolbox.Direction;
import net.roryclaasen.lost.toolbox.Vector2i;

public class Chaser extends Mob {

	private int chasseDistance = 7 * 32;
	private SpriteAnimated animDown = new SpriteAnimated(SpriteSheet.chaser, new int[]{0, 1, 2});
	private SpriteAnimated animLeft = new SpriteAnimated(SpriteSheet.chaser, new int[]{3, 4, 5});
	private SpriteAnimated animRight = new SpriteAnimated(SpriteSheet.chaser, new int[]{6, 7, 8});
	private SpriteAnimated animUp = new SpriteAnimated(SpriteSheet.chaser, new int[]{9, 10, 11});

	private List<Node> path = null;

	public Chaser(Level level) {
		super(level, SpriteSheet.chaser.get(1));
		anim = animDown;
		anim.setSprite(1);
		healthMax = 20;
		health = 20;
	}

	@Override
	public void update() {
		time++;
		if (canRoam()) {
			int xa = 0, ya = 0;
			if (!moving) {
				anim.setInterval(7);
				anim.setSprite(1);

				Vector2i start = getPosition().getCords();
				Vector2i destination = level.getPlayer().getPosition().getCords();
				if (getDistanceFrom(level.getPlayer()) < chasseDistance || chasseDistance == -1) {
					if (time % 10 == 0) path = level.findPath(start, destination);
					if (path != null) {
						if (path.size() > 0 && (path.size() <= chasseDistance || chasseDistance == -1)) {
							Vector2i vec = path.get(path.size() - 1).tile;
							int vecX = vec.getX() * 32;
							int vecY = vec.getY() * 32;
							if (x < vecX) xa += speed;
							if (x > vecX) xa -= speed;
							if (y < vecY) ya += speed;
							if (y > vecY) ya -= speed;
							if ((x < vecX && x - speed > vecX) || (x < vecX && x + speed > vecX)) x = vecX;
							if ((y < vecY && y + speed > vecY) || (y < vecY && y - speed > vecY)) y = vecY;
						}
					}
					move(xa, ya);
				}
				if (getDistanceFrom(level.getPlayer()) < 50) {
					if (attackTime == 0) level.attack(this, level.getPlayer());
					attackTime++;
					attackTime %= attackSpeed;
				}
			}
			if (getDirection() == Direction.SOUTH) anim = animDown;
			if (getDirection() == Direction.NORTH) anim = animUp;
			if (getDirection() == Direction.WEST) anim = animLeft;
			if (getDirection() == Direction.EAST) anim = animRight;
			if (anim != null) sprite = anim.getSprite();
		}
	}

	@Override
	public String getName() {
		return "Hunter";
	}
}
