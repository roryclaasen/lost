package net.roryclaasen.lost.level.entity.mob;

import net.roryclaasen.lost.graphics.sprite.SpriteAnimated;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.level.entity.Mob;
import net.roryclaasen.lost.toolbox.Direction;
import net.roryclaasen.lost.toolbox.Vector2i;

public class DummyFollower extends Mob {

	private int radius = 7;
	private SpriteAnimated animDown = new SpriteAnimated(SpriteSheet.dummy, new int[]{12, 13, 14});
	private SpriteAnimated animLeft = new SpriteAnimated(SpriteSheet.dummy, new int[]{15, 16, 17});
	private SpriteAnimated animRight = new SpriteAnimated(SpriteSheet.dummy, new int[]{18, 19, 20});
	private SpriteAnimated animUp = new SpriteAnimated(SpriteSheet.dummy, new int[]{21, 22, 23});

	public DummyFollower(Level level) {
		super(level, SpriteSheet.dummy.get(13));
		anim = animDown;
		healthMax = 10;
		health = 10;
	}

	@Override
	public void update() {
		time++;
		if (canRoam()) {
			if (!moving) {
				anim.setInterval(7);
				anim.setSprite(1);
				if (getTileDistanceFrom(level.getPlayer()) < radius) {
					int xa = 0, ya = 0;
					Vector2i p = level.getPlayer().getPosition().getCords();
					Vector2i c = getPosition().getCords();
					if (c.getX() > p.getX()) xa = -1;
					else if (c.getX() < p.getX()) xa = 1;
					if (c.getY() > p.getY()) ya = -1;
					else if (c.getY() < p.getY()) ya = 1;
					move(xa, ya);
				}
			}
			if (getDirection() == Direction.SOUTH) anim = animDown;
			if (getDirection() == Direction.NORTH) anim = animUp;
			if (getDirection() == Direction.WEST) anim = animLeft;
			if (getDirection() == Direction.EAST) anim = animRight;
			if (anim != null) sprite = anim.getSprite();
		}
	}

	@Override
	public String getName() {
		return "Dummy Follower";
	}
}
