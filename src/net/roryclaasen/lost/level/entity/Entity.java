package net.roryclaasen.lost.level.entity;

import java.util.Random;

import net.roryclaasen.lost.graphics.sprite.Sprite;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.level.entity.mob.player.Player;
import net.roryclaasen.lost.options.Options;
import net.roryclaasen.lost.toolbox.TileCords;
import net.roryclaasen.lost.toolbox.Vector2i;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public abstract class Entity {

	protected final Random random = new Random();

	protected Level level;
	protected Sprite sprite;
	protected double x, y;

	private boolean roaming;

	protected final int speed = 1;
	protected float opacity = 1F;

	protected Color boxColor;

	private boolean removed = false;

	public Entity(Level level) {
		this.level = level;
	}

	public Entity(Level level, Sprite sprite) {
		this.level = level;
		this.sprite = sprite;
	}

	protected void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}

	public Sprite getSprite() {
		return sprite;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getX() {
		return (int) x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getY() {
		return (int) y;
	}
	
	public void setXdouble(double x) {
		this.x = x;
	}

	public double getXdouble() {
		return  x;
	}

	public void setYdouble(double y) {
		this.y = y;
	}

	public double getYdouble() {
		return y;
	}

	public abstract void update();

	public abstract void render(Graphics g, int xOffset, int yOffset);

	public void remove() {
		opacity = 0F;
		removed = true;
	}

	public boolean isRemoved() {
		return removed;
	}

	public Entity setAbsolutePosition(int x, int y) {
		this.x = x;
		this.y = y;
		return this;
	}

	public Entity setAbsolutePosition(Vector2i location) {
		return setAbsolutePosition(location.getX(), location.getY());
	}

	public Vector2i getAbsolutePosition() {
		return new Vector2i((int) x, (int) y);
	}

	public Entity setPosition(TileCords cords) {
		x = cords.getRenderCords().getX();
		y = cords.getRenderCords().getY();
		return this;
	}

	public TileCords getPosition() {
		return new TileCords(TileCords.getCordsFromRender((int) x, (int) y));
	}

	public boolean canRoam() {
		return (roaming && Options.mobsCanMove.get()) || (this instanceof Player);
	}

	public Entity setRoaming(boolean roaming) {
		this.roaming = roaming;
		return this;
	}

	public double getDistanceFrom(Entity other) {
		double run = x - other.getX();
		double rise = y - other.getY();
		return Math.floor(Math.sqrt(Math.pow(run, 2) + Math.pow(rise, 2)));
	}

	public int getTileDistanceFrom(Entity other) {
		double run = x - other.getX();
		double rise = y - other.getY();
		return (int) Math.floor(Math.sqrt(Math.pow(run, 2) + Math.pow(rise, 2)) / 32);
	}

	public abstract String getName();
}
