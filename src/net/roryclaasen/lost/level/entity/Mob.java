package net.roryclaasen.lost.level.entity;

import java.util.ArrayList;
import java.util.List;

import net.roryclaasen.lost.graphics.item.ItemStack;
import net.roryclaasen.lost.graphics.level.HealthBar;
import net.roryclaasen.lost.graphics.sprite.Sprite;
import net.roryclaasen.lost.graphics.sprite.SpriteAnimated;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.graphics.tile.TileBase;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.level.entity.mob.player.Player;
import net.roryclaasen.lost.soundengine.SoundManager;
import net.roryclaasen.lost.toolbox.Arguments;
import net.roryclaasen.lost.toolbox.Direction;
import net.roryclaasen.lost.toolbox.TileCords;
import net.roryclaasen.lost.toolbox.list.Colors;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public abstract class Mob extends Entity {

	protected SpriteAnimated anim;
	protected int time;

	protected double healthMax = 20;
	protected double health = 20;
	protected int attackSpeed = 30, attackTime;
	protected int dammage = 1;

	private boolean dead = false;

	protected List<ItemStack> loot = new ArrayList<ItemStack>();

	private HealthBar healthBar;

	private Direction dir = Direction.CENTER;

	protected boolean moving;

	private int movingX = 0;
	private int movingY = 0;

	private int soundWaitTime = 10, soundFootChannel;

	private Mob killedBy;

	public Mob(Level level, Sprite sprite) {
		super(level, sprite);
		healthBar = new HealthBar(this);
		boxColor = Colors.boxMob;
	}

	public Mob(Level level) {
		super(level);
		healthBar = new HealthBar(this);
		boxColor = Colors.boxMob;
	}

	public abstract void update();

	public void rootUpdate() {
		healthBar.update();
		updateMove();
	}

	public void render(Graphics g, int x_off, int y_off) {
		if (dead) {
			if (opacity <= 0.05) remove();
			else opacity -= 0.075F;
			// ((Graphics2D) g).setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, opacity));
		}
		if (sprite != null) sprite.getImage().draw(x_off + getX(), y_off + getY());
		healthBar.render(g, x_off, y_off);
		if (Arguments.showBounds()) {
			Color c = g.getColor();
			g.setColor(boxColor);
			g.drawRect(x_off + getX(), y_off + getY(), 32, 32);
			g.setColor(c);
		}
		// if (dead) ((Graphics2D) g).setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1F));
	}

	public Direction getDirection() {
		return dir;
	}

	public Mob setDirection(Direction direction) {
		this.dir = direction;
		return this;
	}

	public void move(int xa, int ya) {
		if (!dead) {
			if (canRoam()) {
				if (xa != 0 && ya != 0) {
					move(xa, 0);
					move(0, ya);
					return;
				}

				if (xa == 0 && ya == 0) return;
				boolean ground = level.getTile(TileCords.getCordsFromRender(getX() + (xa * 32), getY() + (ya * 32))).isSolid();
				boolean blocks = level.getTile("Blocks", TileCords.getCordsFromRender(getX() + (xa * 32), getY() + (ya * 32))).isSolid();
				if (ground || blocks) return;
				else {
					movingX = xa;
					x += movingX;

					movingY = ya;
					y += movingY;
				}
			}
		}
	}

	public TileBase getCurrentTile() {
		return level.getTile(TileCords.getCordsFromRender(getX(), getY()));
	}

	private void updateMove() {
		if (!dead) {
			if (canRoam()) {
				if (movingX != 0 || movingY != 0) moving = true;
				if (movingX != 0) {
					if (x % 32 != 0) x += movingX;
					else stopped();
				} else if (movingY != 0) {
					if (y % 32 != 0) y += movingY;
					else stopped();
				}
				if (moving) {
					if (anim != null) anim.update();
					if (movingX < 0) dir = Direction.WEST;
					if (movingX > 0) dir = Direction.EAST;
					if (movingY < 0) dir = Direction.NORTH;
					if (movingY > 0) dir = Direction.SOUTH;
					
					if (!SoundManager.footstep01.playing(soundFootChannel) && time % (random.nextInt(soundWaitTime) + soundWaitTime) == 0) {
						if (this instanceof Player) soundFootChannel = 0;
						else soundFootChannel = SoundManager.footstep01.getFirstSpace(1);

						SoundManager.playIn(SoundManager.footstep01, soundFootChannel);
					}
					if (time % 3 == 0) level.add(new Particle(level, x + 16, y + 30, 15, Particle.LARGE, SpriteSheet.sheet1.get(getCurrentTile().getID())));
				}
			}
		}
	}

	public void stopped() {
		moving = false;
		movingY = 0;
		movingX = 0;
	}

	public void pickUp() {
		if (!dead) {
			if (level.isStandingOnItem(this)) {
				if (level.getItem(getPosition()).canPickUp()) {
					if (this instanceof Player) {
						if (((Player) this).pickUp(level.getItem(getPosition()).getItemStack())) {
							level.getItem(getPosition()).pickUp();
							level.getUserInterface().newMessage("Player Picked up " + level.getItem(getPosition()).getItemStack().getName(), 1);
						}
					} else {
						try {
							level.getItem(getPosition()).pickUp();
							loot.add(level.getItem(getPosition()).getItemStack());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	public boolean isMoving() {
		return moving;
	}

	public int getAttackSpeed() {
		return attackSpeed;
	}

	public double getHealth() {
		return health;
	}

	public double getMaxHealth() {
		return healthMax;
	}

	public int getDammage() {
		return dammage;
	}

	public void kill(Mob mob) {
		killedBy = mob;
		level.drop(getPosition(), loot);
		dead = true;
	}

	public boolean isDead() {
		return dead;
	}

	public void takeDammage(Mob mob) {
		if (!dead) health -= mob.getDammage();
		if (health <= 0) kill(mob);
	}

	public void takeDammage(int dammage) {
		if (!dead) health -= dammage;
		if (health <= 0) kill(null);
	}

	public int dealDammage() {
		return dammage;
	}

	public Mob killedBy() {
		return killedBy;
	}

	public Mob setLoot(List<ItemStack> loot) {
		this.loot = loot;
		return this;
	}

	public void addToLoot(ItemStack item) {
		this.loot.add(item);
	}

	public void addToLoot(List<ItemStack> loot) {
		this.loot.addAll(loot);
	}

	public List<ItemStack> getLoot() {
		return loot;
	}
}
