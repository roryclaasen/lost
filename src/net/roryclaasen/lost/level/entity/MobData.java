package net.roryclaasen.lost.level.entity;

import java.util.List;

import net.gogo98901.log.Log;
import net.roryclaasen.lost.graphics.item.ItemStack;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.level.entity.mob.Chaser;
import net.roryclaasen.lost.level.entity.mob.Dummy;
import net.roryclaasen.lost.level.entity.mob.DummyFollower;
import net.roryclaasen.lost.level.entity.mob.Guard;
import net.roryclaasen.lost.level.entity.mob.player.Player;
import net.roryclaasen.lost.toolbox.Direction;
import net.roryclaasen.lost.toolbox.TileCords;

public class MobData {
	public static final int MAX = 64;

	public static final int mobPlayer = 0;
	public static final int mobDummy = 1;
	public static final int mobDummyFollower = 2;
	public static final int mobChaser = 8;
	public static final int mobGuard = 9;

	public static Mob newMob(Level level, int id, TileCords tileCords, Direction dir, boolean roam, List<ItemStack> loot) {
		if (id == mobPlayer) {
			Player p = new Player(level);
			p.setPosition(tileCords);
			p.setInventory(loot);
			p.setRoaming(roam);
			//p.setDirection(dir);
			return p;
		} else {
			if (id == mobDummy) return ((Mob) new Dummy(level).setPosition(tileCords).setRoaming(roam))/*.setDirection(dir)*/.setLoot(loot);
			if (id == mobDummyFollower) return ((Mob) new DummyFollower(level).setPosition(tileCords).setRoaming(roam))/*.setDirection(dir)*/.setLoot(loot);
			if (id == mobChaser) return ((Mob) new Chaser(level).setPosition(tileCords).setRoaming(roam))/*.setDirection(dir)*/.setLoot(loot);
			if (id == mobGuard) return ((Mob) new Guard(level).setPosition(tileCords).setRoaming(roam))/*.setDirection(dir)*/.setLoot(loot);
		}
		Log.warn("No mob with id of " + id);
		return null;
	}
}
