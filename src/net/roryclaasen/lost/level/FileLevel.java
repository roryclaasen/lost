package net.roryclaasen.lost.level;

import java.util.ArrayList;
import java.util.List;

import net.gogo98901.log.Log;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class FileLevel {
	private int width, height;
	private int tileWidth, tileHeight;

	private List<Layer> layers = new ArrayList<Layer>();

	private int ground_layer = 0;

	public FileLevel(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public void setLayers(JSONArray layersList) {
		for (int i = 0; i < layersList.size(); i++) {
			Object object = layersList.get(i);
			Layer layer = new Layer();
			JSONObject objLayer = (JSONObject) object;
			layer.setName(objLayer.get("name").toString());
			layer.setWidth(Integer.parseInt(objLayer.get("width").toString()));
			layer.setHeight(Integer.parseInt(objLayer.get("height").toString()));
			if (layer.getName().toLowerCase().equals("ground")) ground_layer = i;
			JSONArray data = (JSONArray) objLayer.get("data");
			int[] intData = new int[data.size()];
			for (int i2 = 0; i2 < data.size(); i2++) {
				int id = Integer.parseInt(data.get(i2).toString()) - 1;
				intData[i2] = id;
			}
			layer.setData(intData);
			layers.add(layer);
			Log.info("Loaded Level Layer [" + layer.getName() + "]");
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getTileWidth() {
		return tileWidth;
	}

	public int getTileHeight() {
		return tileHeight;
	}

	public List<Layer> getLayers() {
		return layers;
	}

	public Layer getGroundLayer() {
		return layers.get(ground_layer);
	}

	public Layer getLayer(String string) {
		for (Layer layer : layers) {
			if (layer.getName().toLowerCase().equals(string.toLowerCase())) return layer;
		}
		return null;
	}

	public int[] getLayerData(String name) {
		return getLayer(name).data;
	}
}
