package net.roryclaasen.lost.level;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.gogo98901.log.Log;
import net.gogo98901.reader.StaticReader;
import net.roryclaasen.lost.graphics.item.ItemDroped;
import net.roryclaasen.lost.graphics.item.ItemStack;
import net.roryclaasen.lost.graphics.item.Items;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.graphics.tile.TileBase;
import net.roryclaasen.lost.graphics.tile.TileBaseAnimated;
import net.roryclaasen.lost.graphics.tile.Tiles;
import net.roryclaasen.lost.graphics.tile.tiles.TileBreakable;
import net.roryclaasen.lost.graphics.ui.UserInterface;
import net.roryclaasen.lost.level.entity.Entity;
import net.roryclaasen.lost.level.entity.Mob;
import net.roryclaasen.lost.level.entity.MobData;
import net.roryclaasen.lost.level.entity.Obstruction;
import net.roryclaasen.lost.level.entity.Particle;
import net.roryclaasen.lost.level.entity.mob.player.Player;
import net.roryclaasen.lost.options.Options;
import net.roryclaasen.lost.state.GameState;
import net.roryclaasen.lost.toolbox.Arguments;
import net.roryclaasen.lost.toolbox.Direction;
import net.roryclaasen.lost.toolbox.TileCords;
import net.roryclaasen.lost.toolbox.Vector2i;
import net.roryclaasen.lost.toolbox.list.Files;
import net.roryclaasen.lost.window.DisplayWindow;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Level {

	private String levelName;
	private GameState state;
	private boolean loaded = false;
	private JSONParser parser = new JSONParser();
	private Level level;

	private FileLevel fileLevel;

	public int width, height;

	private List<Entity> entities = new ArrayList<Entity>();
	private List<Obstruction> obstruction = new ArrayList<Obstruction>();
	private List<Particle> particles = new ArrayList<Particle>();
	private List<Mob> mobs = new ArrayList<Mob>();
	private List<ItemDroped> items = new ArrayList<ItemDroped>();
	private Player player;

	private int x_off = 0;
	private int y_off = 0;

	private int renderDefault = 10, renderRadius = renderDefault;

	private boolean expandRadius = false;

	public Comparator<Node> nodeSorter = new Comparator<Node>() {

		public int compare(Node n0, Node n1) {
			if (n1.fCost < n0.fCost) return +1;
			if (n1.fCost > n0.fCost) return -1;
			return 0;
		}
	};

	public Level(GameState state) {
		this.state = state;
		level = this;
	}

	public void reLoadLevel(boolean keepPlayer) {
		if (Arguments.isDeveloper()) {
			Log.info("Initializing re-load level...");
			TileCords oldPos = player.getPosition();
			loadLevel(Files.saveLevel1Folder);
			if (keepPlayer) player.setPosition(oldPos);
			Log.info("Initializing re-load level... Finnished");
		}
	}

	public void loadLevel(File levelFolder) {
		levelName = levelFolder.getName();
		Log.info("Loading level [" + levelName + "]...");
		entities.clear();
		mobs.clear();
		items.clear();
		player = null;
		try {
			JSONObject world = (JSONObject) parser.parse(StaticReader.read(levelFolder.getAbsolutePath().toString() + "/world.json", false));
			int width = Integer.parseInt(world.get("width").toString());
			int height = Integer.parseInt(world.get("height").toString());
			fileLevel = new FileLevel(width, height);
			fileLevel.setLayers((JSONArray) world.get("layers"));
			this.width = fileLevel.getGroundLayer().getWidth();
			this.height = fileLevel.getGroundLayer().getHeight();

			loadEntityData(levelFolder);

			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					if (getTile("Blocks", x, y) instanceof TileBreakable) {
						add(new Obstruction(x, y, ((TileBreakable) getTile("Blocks", x, y)).getHealth()).addBlockListener(((TileBreakable) getTile("Blocks", x, y)).getListener()));
					}
				}
			}
		} catch (ParseException e) {
			Log.info("Loading level [" + levelName + "]... FAILED");
			Log.stackTrace(e);
			return;
		}
		Log.info("Loading level [" + levelName + "]... SUCCESS");
		loaded = true;
	}

	private void loadEntityData(File file) throws ParseException {
		Log.info("Loading entity data");
		File entitiesFile = new File(file.getAbsolutePath().toString() + "/entities.json");
		JSONObject entities = (JSONObject) parser.parse(StaticReader.read(entitiesFile.getAbsolutePath().toString(), false));
		Log.info("Loading Mobs");
		{// Mobs
			try {
				JSONArray mobs = (JSONArray) entities.get("mobs");
				for (int m = 0; m < mobs.size(); m++) {
					JSONObject entity = (JSONObject) mobs.get(m);
					int id = Integer.parseInt(entity.get("id").toString());
					if (id == 0) {
						Log.warn("Player Data found in [" + entitiesFile.getName() + "], Data ignored!");
						continue;
					}
					int x = Integer.parseInt(entity.get("x").toString());
					int y = Integer.parseInt(entity.get("y").toString());
					List<ItemStack> loot = new ArrayList<ItemStack>();
					boolean roam = (boolean) entity.get("roam");
					JSONArray inv = (JSONArray) entity.get("loot");
					for (int i = 0; i < inv.size(); i++) {
						try {
							JSONObject item = (JSONObject) inv.get(i);
							int itemId = Integer.parseInt(item.get("id").toString());
							int itemQu = Integer.parseInt(item.get("no").toString());
							loot.add(new ItemStack(Items.getItem(itemId), itemQu));
						} catch (Exception itemError) {
							Log.warn("Did not load item to mob");
						}
					}
					Direction dir = Direction.values()[Integer.parseInt(entity.get("dir").toString())];
					add(MobData.newMob(level, id, new TileCords(x, y), dir, roam, loot));
				}
			} catch (Exception mobError) {
				Log.warn("Mob loading error");
				Log.stackTrace(mobError);
			}
		}
		Log.info("Finished Mobs");
		Log.info("Loading Items");
		{// Items
			try {
				JSONArray items = (JSONArray) entities.get("items");
				for (int i = 0; i < items.size(); i++) {
					JSONObject entity = (JSONObject) items.get(i);
					int id = Integer.parseInt(entity.get("id").toString());
					int x = Integer.parseInt(entity.get("x").toString());
					int y = Integer.parseInt(entity.get("y").toString());
					int q = Integer.parseInt(entity.get("q").toString());
					add(new ItemDroped(level, new ItemStack(Items.getItem(id), q), new TileCords(x, y)));
				}
			} catch (Exception mobError) {
				Log.warn("Item loading error");
				Log.stackTrace(mobError);
			}
		}
		Log.info("Finished Items");
		{// Player
			Log.info("Loading Player data");
			try {
				File playerFile = new File(file.getAbsolutePath().toString() + "/player.json");
				JSONObject player = (JSONObject) parser.parse(StaticReader.read(playerFile.getAbsolutePath().toString(), false));
				int x = Integer.parseInt(player.get("x").toString());
				int y = Integer.parseInt(player.get("y").toString());

				List<ItemStack> loot = new ArrayList<ItemStack>();
				JSONArray inv = (JSONArray) player.get("loot");
				for (int i = 0; i < inv.size(); i++) {
					try {
						JSONObject item = (JSONObject) inv.get(i);
						int itemId = Integer.parseInt(item.get("id").toString());
						if (itemId == -1) {
							loot.add(null);
							continue;
						}
						int itemQu = Integer.parseInt(item.get("no").toString());
						loot.add(new ItemStack(Items.getItem(itemId), itemQu));
					} catch (Exception itemError) {
						Log.warn("Did not load item to player");
					}
				}
				Direction dir = Direction.values()[Integer.parseInt(player.get("dir").toString())];
				add(MobData.newMob(level, 0, new TileCords(x, y), dir, true, loot));
			} catch (Exception playerError) {
				Log.severe("Player loading error");
				Log.stackTrace(net.gogo98901.log.Level.SEVERE, playerError);
			}
			updateOffset();
			Log.info("Finished Player");
		}
	}

	public void render(Graphics g) {
		if (loaded) {
			try {
				int[] data = fileLevel.getGroundLayer().getData();
				for (int y = player.getPosition().getCords().getY() - renderRadius; y < player.getPosition().getCords().getY() + renderRadius + 1; y++) {
					for (int x = player.getPosition().getCords().getX() - renderRadius; x < player.getPosition().getCords().getX() + renderRadius + 1; x++) {
						if (x < 0 || y < 0 || x >= width || y >= height) continue;
						TileBase tile = Tiles.getTile(data[x + y * fileLevel.getGroundLayer().getWidth()]);
						if (tile instanceof TileBaseAnimated) ((TileBaseAnimated) tile).render(g, x, y);
						else tile.render(g, x_off + x * 32, y_off + y * 32);
					}
				}
			} catch (Exception e) {
				Log.warn("Could not render the world level... 'Ground'... " + e);
				e.printStackTrace();
			}
			try {
				int[] data = fileLevel.getLayerData("Ground Overlay");
				for (int y = player.getPosition().getCords().getY() - renderRadius; y < player.getPosition().getCords().getY() + renderRadius + 1; y++) {
					for (int x = player.getPosition().getCords().getX() - renderRadius; x < player.getPosition().getCords().getX() + renderRadius + 1; x++) {
						if (x < 0 || y < 0 || x >= width || y >= height) continue;
						TileBase tile = Tiles.getTile(data[x + y * fileLevel.getGroundLayer().getWidth()]);
						if (tile != Tiles.voidTile) tile.render(g, x_off + x * 32, y_off + y * 32);
					}
				}
			} catch (Exception e) {
				Log.warn("Could not render the world level... 'Ground'... " + e);
				e.printStackTrace();
			}
			for (ItemDroped item : items) {
				int x = item.getX() * 32;
				int y = item.getY() * 32;
				if (y > player.getY() - renderRadius * 32 - 32 && y < player.getY() + renderRadius * 32) {
					if (x > player.getX() - renderRadius * 32 - 32 && x < player.getX() + renderRadius * 32) {
						item.render(g, x_off, y_off);
					}
				}
			}
			try {
				int[] data = fileLevel.getLayerData("Blocks");
				for (int y = player.getPosition().getCords().getY() - renderRadius; y < player.getPosition().getCords().getY() + renderRadius + 1; y++) {
					for (int x = player.getPosition().getCords().getX() - renderRadius; x < player.getPosition().getCords().getX() + renderRadius + 1; x++) {
						if (x < 0 || y < 0 || x >= width || y >= height) continue;
						TileBase tile = Tiles.getTile(data[x + y * fileLevel.getGroundLayer().getWidth()]);
						if (tile != Tiles.voidTile) {
							if (tile instanceof TileBaseAnimated) ((TileBaseAnimated) tile).render(g, x, y);
							else tile.render(g, x_off + x * 32, y_off + y * 32);
						}
					}
				}
			} catch (Exception e) {
				Log.warn("Could not render the world level... 'Ground'... " + e);
				e.printStackTrace();
			}
			for (Entity entity : entities) {
				int x = entity.getX();
				int y = entity.getY();
				if (y > player.getY() - renderRadius * 32 - 32 && y < player.getY() + renderRadius * 32) {
					if (x > player.getX() - renderRadius * 32 - 32 && x < player.getX() + renderRadius * 32) {
						entity.render(g, x_off, y_off);
					}

				}
			}
			if (Options.showParticles.get()) {
				for (Particle particle : particles) {
					int x = particle.getX();
					int y = particle.getY();
					if (y > player.getY() - renderRadius * 32 - 32 && y < player.getY() + renderRadius * 32) {
						if (x > player.getX() - renderRadius * 32 - 32 && x < player.getX() + renderRadius * 32) {
							particle.render(g, x_off, y_off);
						}
					}
				}
			}
			for (Mob mob : mobs) {
				int x = mob.getX();
				int y = mob.getY();
				if (y > player.getY() - renderRadius * 32 - 32 && y < player.getY() + renderRadius * 32) {
					if (x > player.getX() - renderRadius * 32 - 32 && x < player.getX() + renderRadius * 32) {
						mob.render(g, x_off, y_off);
					}
				}
			}
			if (player != null) {
				player.renderExtra(g, x_off, y_off);
				player.render(g, x_off, y_off);
			}
			try {
				for (int i = 0; i < fileLevel.getLayers().size(); i++) {
					Layer layer = fileLevel.getLayers().get(i);
					if (layer == fileLevel.getGroundLayer()) continue;
					if (layer.getName().toLowerCase().startsWith("ground")) continue;
					if (layer.getName().toLowerCase().startsWith("blocks")) continue;
					int[] data = layer.getData();
					for (int y = player.getPosition().getCords().getY() - renderRadius; y < player.getPosition().getCords().getY() + renderRadius + 1; y++) {
						for (int x = player.getPosition().getCords().getX() - renderRadius; x < player.getPosition().getCords().getX() + renderRadius + 1; x++) {
							if (x < 0 || y < 0 || x >= width || y >= height) continue;
							TileBase tile = Tiles.getTile(data[x + y * layer.getWidth()]);
							if (tile != Tiles.voidTile) tile.render(g, x_off + x * 32, y_off + y * 32);
						}
					}
				}
			} catch (Exception e) {
				Log.warn("Could not render the world level... 'Ground'... " + e);
				e.printStackTrace();
			}
			if (Arguments.showGridCords()) {
				Color c = g.getColor();
				g.setColor(Color.white);
				for (int y = player.getPosition().getCords().getY() - renderRadius; y < player.getPosition().getCords().getY() + renderRadius + 1; y++) {
					for (int x = player.getPosition().getCords().getX() - renderRadius; x < player.getPosition().getCords().getX() + renderRadius + 1; x++) {
						if (x < 0 || y < 0 || x >= width || y >= height) continue;
						g.drawString(x + "," + y, x_off + x * 32, y_off + y * 32);
					}
				}
				g.setColor(c);
			}
			if (player != null) player.renderShaddow(g, x_off, y_off);
		} else {
			Log.warn("level is not loaded! broken?");
		}
	}

	public void update() {
		if (loaded) {
			if (player != null) {
				if (expandRadius) {
					if (player.getPosition().getCords().getX() < 10 || player.getPosition().getCords().getY() < 10) {
						if (player.getPosition().getCords().getX() < 10) renderRadius = renderDefault + (10 - player.getPosition().getCords().getX());
						else if (player.getPosition().getCords().getY() < 10) renderRadius = renderDefault + (10 - player.getPosition().getCords().getY());
					} else renderRadius = renderDefault;
				}
				player.rootUpdate();
				player.update();
				updateOffset();
			}
			for (ItemDroped item : items) {
				item.update();
			}
			for (Mob mob : mobs) {
				mob.rootUpdate();
				mob.update();
			}
			if (Options.showParticles.get()) {
				for (Particle particle : particles) {
					particle.update();
				}
			} else particles.clear();
			for (Entity entity : entities) {
				entity.update();
			}
			remove();

			for (int i = 0; i < fileLevel.getLayers().size(); i++) {
				Layer layer = fileLevel.getLayers().get(i);
				int[] data = layer.getData();
				for (int y = player.getPosition().getCords().getY() - renderRadius; y < player.getPosition().getCords().getY() + renderRadius + 1; y++) {
					for (int x = player.getPosition().getCords().getX() - renderRadius; x < player.getPosition().getCords().getX() + renderRadius + 1; x++) {
						if (x < 0 || y < 0 || x >= width || y >= height) continue;
						TileBase tile = Tiles.getTile(data[x + y * layer.getWidth()]);
						if (tile instanceof TileBaseAnimated) ((TileBaseAnimated) tile).update();
					}
				}
			}
		}
	}

	public void updateOffset() {
		x_off = -(player.getX() - (DisplayWindow.getWidth() / 2)) - 16;
		y_off = -(player.getY() - (DisplayWindow.getHeight() / 2)) - 16;

		if (x_off > 0) x_off = 0;
		if (y_off > 0) y_off = 0;
		// if (x_off < width * 32) x_off = width * 32;
		// if (y_off < height * 32) y_off = height * 32;
	}

	public void add(Entity entity) {
		if (entity instanceof Player) {
			if (player == null) player = (Player) entity;
		} else if (entity instanceof Obstruction) {
			obstruction.add((Obstruction) entity);
		} else if (entity instanceof Particle) {
			particles.add((Particle) entity);
		} else if (entity instanceof Mob) {
			mobs.add((Mob) entity);
		} else {
			entities.add(entity);
		}
	}

	public void add(ItemDroped item) {
		items.add(item);
	}

	public void remove() {
		for (int i = 0; i < mobs.size(); i++) {
			if (mobs.get(i).isRemoved()) mobs.remove(i);
		}
		for (int i = 0; i < entities.size(); i++) {
			if (entities.get(i).isRemoved()) entities.remove(i);
		}
		for (int i = 0; i < entities.size(); i++) {
			if (obstruction.get(i).isRemoved()) obstruction.remove(i);
		}
		for (int i = 0; i < particles.size(); i++) {
			if (particles.get(i).isRemoved()) particles.remove(i);
		}
		for (int i = 0; i < items.size(); i++) {
			if (items.get(i).isRemoved()) items.remove(i);
		}
		if (player.isRemoved()) player = null;
	}

	public Obstruction getObstruction(Vector2i pos) {
		return getObstruction(pos.getX(), pos.getY());
	}

	public Obstruction getObstruction(int x, int y) {
		for (Obstruction obj : obstruction) {
			if (obj.getX() == x && obj.getY() == y) return obj;
		}
		return null;
	}

	public TileBase getTile(TileCords tile) {
		return getTile(tile.getCords());
	}

	public TileBase getTile(Vector2i vector) {
		return getTile(vector.getX(), vector.getY());
	}

	public TileBase getTile(String layer, Vector2i vector) {
		return getTile(layer, vector.getX(), vector.getY());
	}

	public TileBase getTile(int x, int y) {
		return getTile("Ground", x, y);
	}

	public TileBase getTile(String layer, int x, int y) {
		if (x < 0 || y < 0 || x >= width || y >= height) return Tiles.voidTile;
		return Tiles.getTile(fileLevel.getLayer(layer).getData()[x + y * fileLevel.getGroundLayer().getWidth()]);
	}

	public void setTile(int x, int y, TileBase tile) {
		setTile("Ground", x, y, tile.getID());
	}

	public void setTile(int x, int y, int id) {
		setTile("Ground", x, y, id);
	}

	public void setTile(String layer, Vector2i pos, int id) {
		setTile(layer, pos.getX(), pos.getY(), id);
	}

	public void setTile(String layer, int x, int y, TileBase tile) {
		setTile(layer, x, y, tile.getID());
	}

	public void setTile(String layer, Vector2i pos, TileBase tile) {
		setTile(layer, pos.getX(), pos.getY(), tile.getID());
	}

	public void setTile(String layer, int x, int y, int id) {
		if (x < 0 || y < 0 || x >= width || y >= height) {
			Log.stackTrace(new Exception("Tile x, y out of bounds"));
			return;
		}
		fileLevel.getLayerData(layer)[x + y * fileLevel.getGroundLayer().getWidth()] = id;
	}

	public Player getPlayer() {
		return player;
	}

	public List<Entity> getEntities() {
		return entities;
	}

	public List<Mob> getMobs() {
		return mobs;
	}

	public int sizeMobs() {
		return mobs.size();
	}

	public int sizeEntities() {
		return entities.size();
	}

	public boolean isStandingOnItem(Entity entity) {
		for (ItemDroped item : items) {
			if (item.getTileCords().getCords().getX() == entity.getPosition().getCords().getX()) {
				if (item.getTileCords().getCords().getY() == entity.getPosition().getCords().getY()) {
					return true;
				}

			}
		}
		return false;
	}

	public ItemDroped getItem(TileCords cords) {
		for (ItemDroped item : items) {
			if (item.getTileCords().getCords().getX() == cords.getCords().getX()) {
				if (item.getTileCords().getCords().getY() == cords.getCords().getY()) {
					return item;
				}
			}
		}
		return null;
	}

	public void drop(TileCords cords, List<ItemStack> items) {
		for (ItemStack item : items) {
			this.items.add(new ItemDroped(level, item, cords));
		}
	}

	public void attack(Mob mob, Direction dir) {
		int x = mob.getX() + (mob.getSprite().getWidth() / 2);
		int y = mob.getY() + (mob.getSprite().getHeight() / 2);
		if (!(mob instanceof Player)) {
			int playerX = player.getX() + (player.getSprite().getWidth() / 2);
			int playerY = player.getY() + (player.getSprite().getHeight() / 2);
			if (dir == Direction.NORTH) {
				if (playerX > x - (mob.getSprite().getWidth() * 0.75) && playerX < x + (mob.getSprite().getWidth() * 0.75)) {
					if (playerY > y - (mob.getSprite().getHeight() * 0.75) - 32 && playerY < y + (mob.getSprite().getHeight() * 0.75)) {
						player.takeDammage(mob);
					}
				}
			} else if (dir == Direction.EAST) {
				if (playerX > x - (mob.getSprite().getWidth() * 0.75) && playerX < x + (mob.getSprite().getWidth() * 0.75) + 32) {
					if (playerY > y - (mob.getSprite().getHeight() * 0.75) && playerY < y + (mob.getSprite().getHeight() * 0.75)) {
						player.takeDammage(mob);
					}
				}
			} else if (dir == Direction.SOUTH) {
				if (playerX > x - (mob.getSprite().getWidth() * 0.75) && playerX < x + (mob.getSprite().getWidth() * 0.75)) {
					if (playerY > y - (mob.getSprite().getHeight() * 0.75) && playerY < y + (mob.getSprite().getHeight() * 0.75) + 32) {
						player.takeDammage(mob);
					}
				}
			} else if (dir == Direction.WEST) {
				if (playerX > x - (mob.getSprite().getWidth() * 0.75) - 32 && playerX < x + (mob.getSprite().getWidth() * 0.75)) {
					if (playerY > y - (mob.getSprite().getHeight() * 0.75) && playerY < y + (mob.getSprite().getHeight() * 0.75)) {
						player.takeDammage(mob);
					}
				}
			}
		} else {
			// if (player.getHeldItem().canDestroyBlocks()) {
			int dammage = 1;
			if (player.getHeldItem() != null) dammage = player.getHeldItem().getDammage();

			int xa = 0, ya = 0;
			if (dir == Direction.NORTH) ya = -1;
			if (dir == Direction.SOUTH) ya = 1;
			if (dir == Direction.WEST) xa = -1;
			if (dir == Direction.EAST) xa = 1;
			Vector2i pos = TileCords.getCordsFromRender(xa * 32 + player.getX(), ya * 32 + player.getY());
			TileBase tile = getTile("Blocks", pos);
			if (tile instanceof TileBreakable) {
				Obstruction obj = getObstruction(pos);
				obj.doDamage(dammage);
				add(new Particle(this, pos.getX() * 32 + 16, pos.getY() * 32 + 16, 30, Particle.LARGE, 20, SpriteSheet.sheet1.get(tile.getID())));
				if (obj.destroyed()) setTile("Blocks", pos, -1);
			}
			// }
		}
		for (Mob vic : mobs) {
			if (vic.equals(mob)) continue;
			int vicX = vic.getX() + (vic.getSprite().getWidth() / 2);
			int vicY = vic.getY() + (vic.getSprite().getHeight() / 2);
			if (dir == Direction.NORTH) {
				if (vicX > x - (mob.getSprite().getWidth() * 0.75) && vicX < x + (mob.getSprite().getWidth() * 0.75)) {
					if (vicY > y - (mob.getSprite().getHeight() * 0.75) - 32 && vicY < y + (mob.getSprite().getHeight() * 0.75)) {
						vic.takeDammage(mob);
					}
				}
			} else if (dir == Direction.EAST) {
				if (vicX > x - (mob.getSprite().getWidth() * 0.75) && vicX < x + (mob.getSprite().getWidth() * 0.75) + 32) {
					if (vicY > y - (mob.getSprite().getHeight() * 0.75) && vicY < y + (mob.getSprite().getHeight() * 0.75)) {
						vic.takeDammage(mob);
					}
				}
			} else if (dir == Direction.SOUTH) {
				if (vicX > x - (mob.getSprite().getWidth() * 0.75) && vicX < x + (mob.getSprite().getWidth() * 0.75)) {
					if (vicY > y - (mob.getSprite().getHeight() * 0.75) && vicY < y + (mob.getSprite().getHeight() * 0.75) + 32) {
						vic.takeDammage(mob);
					}
				}
			} else if (dir == Direction.WEST) {
				if (vicX > x - (mob.getSprite().getWidth() * 0.75) - 32 && vicX < x + (mob.getSprite().getWidth() * 0.75)) {
					if (vicY > y - (mob.getSprite().getHeight() * 0.75) && vicY < y + (mob.getSprite().getHeight() * 0.75)) {
						vic.takeDammage(mob);
					}
				}
			}
			if (vic.isDead()) getUserInterface().newMessage(vic.getName() + " was killed by " + mob.getName(), 2);
		}
	}

	public void attack(Mob from, TileCords cords) {
		for (Mob mob : mobs) {
			if (mob.equals(from)) continue;
			if (TileCords.equals(mob.getPosition(), cords)) {
				mob.takeDammage(from);
			}
			if (mob.isDead()) getUserInterface().newMessage(mob.getName() + " was killed by " + from.getName(), 2);
		}
		if (!(from instanceof Player)) {
			if (TileCords.equals(player.getPosition(), cords)) {
				player.takeDammage(from);
			}
			if (player.isDead()) getUserInterface().newMessage(player.getName() + " was killed by " + from.getName(), 2);
		}
	}

	public void attack(Mob from, Mob to) {
		if (from != to) {
			to.takeDammage(from.getDammage());
			if (to.isDead()) getUserInterface().newMessage(to.getName() + " was killed by " + to.getName(), 2);
		}
	}

	public void attack(TileCords cords, int dammage) {
		for (Mob mob : mobs) {
			if (TileCords.equals(mob.getPosition(), cords)) {
				mob.takeDammage(dammage);
			}
			if (mob.isDead()) getUserInterface().newMessage(mob.getName() + " has died", 2);
		}
		if (TileCords.equals(player.getPosition(), cords)) {
			player.takeDammage(dammage);
			if (player.isDead()) getUserInterface().newMessage(player.getName() + " has died", 2);
		}
	}

	public void attack(TileCords cords) {
		for (Mob mob : mobs) {
			if (TileCords.equals(mob.getPosition(), cords)) {
				mob.takeDammage(1);
			}
			if (mob.isDead()) getUserInterface().newMessage(mob.getName() + " has died", 2);
		}
	}

	public List<Entity> getEntitiesFromRadius(Entity target, int radius) {
		List<Entity> list = new ArrayList<Entity>();
		for (Entity en : entities) {
			if (en.getTileDistanceFrom(target) <= radius) list.add(en);
		}
		return list;
	}

	public List<Mob> getMobsFromRadius(Entity target, int radius) {
		List<Mob> list = new ArrayList<Mob>();
		for (Mob mb : mobs) {
			if (mb.getTileDistanceFrom(target) <= radius) list.add(mb);
		}
		return list;
	}

	public List<Node> findPath(Vector2i start, Vector2i goal) {
		// Log.info("Going to " + goal.getX() + "," + goal.getY() + "  from " + start.getX() + "," + start.getY());
		List<Node> openList = new ArrayList<Node>();
		List<Node> closedList = new ArrayList<Node>();
		Node current = new Node(start, null, 0, getDistance(start, goal));
		openList.add(current);
		while (openList.size() > 0) {
			Collections.sort(openList, nodeSorter);
			current = openList.get(0);
			if (current.tile.getX() == goal.getX() && current.tile.getY() == goal.getY()) {
				List<Node> path = new ArrayList<Node>();
				while (current.parent != null) {
					path.add(current);
					current = current.parent;
				}
				openList.clear();
				closedList.clear();
				return path;
			}
			openList.remove(current);
			closedList.add(current);
			for (int i = 0; i < 4; i++) {
				int x = current.tile.getX();
				int y = current.tile.getY();
				int xi = 0, yi = 0;
				if (i == 0) yi = -1;
				if (i == 1) xi = 1;
				if (i == 2) yi = 1;
				if (i == 3) xi = -1;
				TileBase at = getTile(x + xi, y + yi);
				if (at == null) continue;
				if (at.isSolid()) continue;
				Vector2i a = new Vector2i(x + xi, y + yi);
				double gCost = current.gCost + (getDistance(current.tile, a) == 1 ? 1 : 0.95);
				double hCost = getDistance(a, goal);
				Node node = new Node(a, current, gCost, hCost);
				if (vecInList(closedList, a) && gCost >= node.gCost) continue;
				if (!vecInList(openList, a) || gCost < node.gCost) openList.add(node);
			}
		}
		closedList.clear();
		return null;
	}

	private boolean vecInList(List<Node> list, Vector2i vector) {
		for (Node n : list) {
			if (n.tile.equals(vector)) return true;
		}
		return false;
	}

	private double getDistance(Vector2i tile, Vector2i goal) {
		double dx = tile.getX() - goal.getX();
		double dy = tile.getY() - goal.getY();
		return Math.sqrt(dx * dx + dy * dy);
	}

	public UserInterface getUserInterface() {
		return state.getUserInterface();
	}
}