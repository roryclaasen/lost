package net.roryclaasen.lost.toolbox;

import net.gogo98901.log.Log;

public class Arguments {
	private static boolean developerMode = false;
	private static boolean buildStart = false;
	private static boolean doIntro = true;
	private static boolean grid = false, gridCords = false;

	public static void check(String[] args) {
		Log.info("Arguments... Checking");
		for (String arg : args) {
			arg = arg.toLowerCase();
			if (arg.startsWith("-")) {
				arg = arg.replaceFirst("-", "");
				{// GENERAL ARGUMENTS
					if (arg.equals("dev") || arg.equals("developer")) developerMode = true;
					if (arg.equals("nointro")) doIntro = false;
				}
			}

			{// DEVELOPER ARGUMENTS
				if (arg.equals("buildstart")) buildStart = true;
				if (arg.equals("grid")) grid = true;
				if (arg.equals("gridcord")) gridCords = true;
			}
		}
	}

	public static boolean isDeveloper() {
		return developerMode;
	}

	public static boolean isBuildStart() {
		return buildStart;
	}

	public static boolean showBounds() {
		return developerMode && grid;
	}

	public static boolean showGridCords() {
		return showBounds() && gridCords;
	}

	public static boolean doIntro() {
		return doIntro;
	}

	public static void toggleBounds() {
		grid = !grid;
	}
	
	public static boolean inEclipse(){
		String inEclipseStr = System.getProperty("runInEclipse");
		return "true".equalsIgnoreCase(inEclipseStr);
	}
}
