package net.roryclaasen.lost.toolbox.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

import javax.swing.JPanel;

import net.gogo98901.log.Log;
import net.gogo98901.util.Loader;
import net.roryclaasen.lost.window.DisplayWindow;

import com.sun.javafx.application.PlatformImpl;

/**
 * An adapted version of FXVideoPlayer.class
 * 
 * @author Ike Gentz
 * @see <a href="http://www.coderanch.com/t/610659/java/java/JMF-JAR">http://www.coderanch.com/</a>
 */
public class VideoPlayer extends JPanel implements /* SystemSpecs, */KeyListener {

	private static final long serialVersionUID = 1L;

	private JFXPanel jfxPanel;
	private MediaView mediaView;
	private boolean playing = false;

	private double scale = 1.0;

	public VideoPlayer(String fileName) {
		initComponents();
		initMediaPlayer(fileName);

		this.setBackground(Color.BLACK);
		this.setSize(DisplayWindow.getWidth(), DisplayWindow.getHeight());
		this.setVisible(true);
	}

	private void initComponents() {
		// The JavaFX 2.x JFXPanel makes the Swing integration seamless
		jfxPanel = new JFXPanel();
		jfxPanel.setBackground(getBackground());
		// Create the JavaFX Scene
		createScene();

		setLayout(new BorderLayout());
		add(jfxPanel, BorderLayout.CENTER);
	}

	private void createScene() {
		// The Scene needs to be created on "FX user thread", NOT on the
		// AWT Event Thread
		PlatformImpl.startup(new Runnable() {

			@Override
			public void run() {
				Group group = new Group();
				Scene scene = new Scene(group, DisplayWindow.getWidth(), DisplayWindow.getHeight());
				jfxPanel.setScene(scene);
				try {
					group.getChildren().add(mediaView);
				} catch (Exception e) {
					// Log.stackTrace(Level.SEVERE, e);
					initComponents();
					return;
				}
			}
		});
	}

	private void initMediaPlayer(String file) {
		Media media = new Media(Loader.getResource(file).toString());
		// Create the player for playing media.
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		// set player property to autoplay

		// Create the view and add it to the Scene.
		mediaView = new MediaView();
		mediaView.setMediaPlayer(mediaPlayer);
		mediaView.setPreserveRatio(true);

		mediaView.setScaleX(/* WIDTH_FACTOR */scale);
		mediaView.setScaleY(/* HEIGHT_FACTOR */scale);
		mediaView.setX((-(1920 - /* SCREEN_WIDTH */DisplayWindow.getWidth())) / 2);
		mediaView.setY((-(1080 - /* SCREEN_HEIGHT */DisplayWindow.getHeight())) / 2);
	}

	public void play() {
		Log.info("Playing file '" + mediaView.getMediaPlayer().getMedia().getSource() + "'");
		playing = true;
		mediaView.getMediaPlayer().play();
	}

	public void play(String file) {
		Media media = new Media(Loader.getResource(file).toString());
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.setOnEndOfMedia(new Runnable() {

			@Override
			public void run() {
				playing = false;
			}
		});
		mediaPlayer.setAutoPlay(true);
		mediaPlayer.pause();
		mediaView.setMediaPlayer(mediaPlayer);
		play();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		e.consume();
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			playing = false;
			e.consume();
		}
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void keyReleased(KeyEvent e) {
		e.consume();
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
	}

	public void update() {
		if (playing) {
			MediaPlayer player = mediaView.getMediaPlayer();
			if (player.getCurrentTime().greaterThanOrEqualTo(player.getMedia().getDuration())) playing = false;
		}
	}

	public boolean isPlaying() {
		return playing;
	}

	public void setScale(double scale) {
		this.scale = scale;
		mediaView.setScaleX(scale);
		mediaView.setScaleY(scale);
	}

	public void stop() {
		playing = false;
		mediaView.getMediaPlayer().stop();
		mediaView.setMediaPlayer(null);
	}

	public MediaPlayer getMediaPlayer() {
		return mediaView.getMediaPlayer();
	}
}