package net.roryclaasen.lost.toolbox;

public enum Direction {
	NORTH, EAST, SOUTH, WEST, CENTER
}
