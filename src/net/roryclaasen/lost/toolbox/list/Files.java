package net.roryclaasen.lost.toolbox.list;

import java.io.File;

public class Files {
	public static final File dataFolder = new File("data/");
	public static final File optionFile = new File(dataFolder + "/options.cfg");

	public static final File saveFolder = new File(dataFolder + "/save/");

	public static final File saveLevel1Folder = new File(saveFolder + "/level1/");
	public static final File checks = new File(dataFolder + "/checks.data");

	/**
	 * This should not need to do anything... If it does then the game may not works!
	 */
	public static boolean createPaths() {
		boolean value = true;
		{
			if (!saveFolder.mkdirs()) value = false;
			if (!dataFolder.mkdirs()) value = false;
			if (!saveLevel1Folder.mkdirs()) value = false;
		}
		return value;
	}
}
