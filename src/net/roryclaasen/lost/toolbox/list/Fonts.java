package net.roryclaasen.lost.toolbox.list;

import java.awt.FontFormatException;
import java.io.IOException;

import net.gogo98901.log.Log;
import net.gogo98901.util.Loader;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.TrueTypeFont;

public class Fonts {

	public static final Font squareo = loadFont("squarefont/Squareo.ttf", 15F);
	public static final Font square18 = loadFont("squarefont/Square.ttf", 18F);
	public static final Font square24 = loadFont("squarefont/Square.ttf", 24F);
	public static final Font square30 = loadFont("squarefont/Square.ttf", 30F);
	public static final Font square40 = loadFont("squarefont/Square.ttf", 40F);
	public static final Font square60 = loadFont("squarefont/Square.ttf", 60F);
	public static final Font square32 = loadFont("squarefont/Square.ttf", 32F);

	public static TrueTypeFont loadFont(String name, float size) {
		String path = "assets/fonts/" + name;
		try {
			java.awt.Font javaFont = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, Loader.getResourceAsStream(path));
			javaFont = javaFont.deriveFont(size);
			return new TrueTypeFont(javaFont, true);
		} catch (FontFormatException | IOException e) {
			Log.stackTrace(e);
		}
		return null;
	}

	public static void drawString(Graphics g, Object text, int x, int y, int opacity) {
		g.getFont().drawString(x + 1, y + 2, text.toString(), new Color(Colors.textBack.getRed(), Colors.textBack.getGreen(), Colors.textBack.getBlue(), opacity));
		g.getFont().drawString(x, y, text.toString(), new Color(Colors.textFront.getRed(), Colors.textFront.getGreen(), Colors.textFront.getBlue(), opacity));
	}

	public static void drawString(Graphics g, Font font, Object text, int x, int y, int opacity) {
		g.setFont(font);
		drawString(g, text, x, y, opacity);
	}

	public static void drawString(Graphics g, Object text, int x, int y) {
		drawString(g, text, x, y, 255);
	}

	public static void drawString(Graphics g, Font font, Object text, int x, int y) {
		drawString(g, font, text, x, y, 255);
	}

	public static void drawFitString(Graphics g, Object text, int x, int y, int width, int height, int opacity) {
		String[] texts = text.toString().split(" ");
		String all = "";
		{
			String line = "";
			for (String word : texts) {
				int wwidth = x + g.getFont().getWidth(line + word + " ");
				if (x + wwidth < x + width) {
					line += word + " ";
					all += word + " ";
				} else {
					all += "\n" + word + " ";
					line = "";
				}
			}
		}
		String[] lines = all.split("\n");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			int posX = x + (width - g.getFont().getWidth(line.toString())) / 2;
			int posY = y + i * (g.getFont().getHeight(line.toString()) + 1);
			drawString(g, line, posX, posY, opacity);
		}
	}

	public static void drawFitString(Graphics g, Font font, Object text, int x, int y, int width, int height) {
		g.setFont(font);
		drawFitString(g, text, x, y, width, height, 255);
	}

	public static void drawCenteredString(Graphics g, Object text, int x, int y, int width, int height, int opacity) {
		int posX = x + (width - g.getFont().getWidth(text.toString())) / 2;
		int posY = y + (height - g.getFont().getLineHeight()) / 2;
		drawString(g, text.toString(), posX, posY, opacity);
	}

	public static void drawCenteredString(Graphics g, Object text, Font font, int x, int y, int width, int height, int opacity) {
		g.setFont(font);
		drawCenteredString(g, text, x, y, width, height, opacity);
	}

	public static void drawCenteredString(Graphics g, Object text, int x, int y, int width, int height) {
		drawCenteredString(g, text, x, y, width, height, 255);
	}

	public static void drawCenteredString(Graphics g, Object text, Font font, int x, int y, int width, int height) {
		drawCenteredString(g, text, font, x, y, width, height, 255);
	}
}
