package net.roryclaasen.lost.toolbox.list;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import net.gogo98901.log.Log;

public class Sites {
	public static Sites github = new Sites("http://github.com/GOGO98901/Lost");
	public static Sites website = new Sites("http://roryclaasen.co.nf");
	
	private String site;

	private Sites(String site) {
		this.site = site;
	}

	public void open() {
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(new URI(site));
				Log.info("Opened  site [" + site + "]");
			} catch (IOException | URISyntaxException e) {
				Log.warn("Could not open site [" + site + "]");
				Log.stackTrace(e);
			}
		}
	}

	public void open(String extra) {
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(new URI(site + "/" + extra));
				Log.info("Opened  site [" + site + "/" + extra + "]");
			} catch (IOException | URISyntaxException e) {
				Log.warn("Could not open site [" + site + "/" + extra + "]");
				Log.stackTrace(e);
			}
		}
	}

	public String getSite() {
		return site;
	}
}
