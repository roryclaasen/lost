package net.roryclaasen.lost.toolbox.list;

public class Strings {
	public static final String _title = "Lost";
	public static final String _author = "Rory Claasen";
	public static final String _build = "Alpha";
	
	public static final String menuPlay = "PLAY";
	public static final String menuOption = "OPTIONS";
	public static final String menuAbout = "ABOUT";
	public static final String menuBack = "BACK";
	public static final String menuExit = "EXIT";
	public static final String menuResume = "Resume";
	public static final String menuGithub = "GitHub";
	public static final String menuWebsite = "Rory Claasen";
	public static final String menuQuit = "QUIT";
	public static final String menuReset= "RESET";
	
	public static final String aboutText = "Lost is a game that I started in September 2015 built from pure java and my own knowledge. Most of the games I (attempt) to make I don't finish (apart from the few) and so this was a target, I wanted to finish it! This is just that, a game that is in progress, a game that will get as far it will go.";
	public static final String aboutTitle = "About the Game";

	public static final String errorFreze = "The game has frozen and will now exit. You may have lost some save progress";
}
