package net.roryclaasen.lost.toolbox.list;

import org.newdawn.slick.Color;


public class Colors {
	public static final Color boxTile = Color.black;
	public static final Color boxPlayer = Color.cyan;
	public static final Color boxMob = Color.pink;
	public static final Color boxButton = Color.green;

	public static final Color textBack = Color.black;
	public static final Color textFront = Color.white;

	public static final Color healthBorder = Color.black;
	public static final Color healthFill = new Color(255, 10, 10);

	public static final Color buttonBack = new Color(40, 40, 40);
	public static final Color buttonHover = new Color(50, 50, 50);
	public static final Color buttonClick = new Color(60, 60, 60);

	public static Color setOpacity(Color color, int opacity) {
		return new Color(color.getRed(), color.getGreen(), color.getBlue(), opacity);
	}
}
