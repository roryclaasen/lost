package net.roryclaasen.lost.toolbox;

public class UpdateLimiter {

	private long lastTime = System.nanoTime();
	private long timer = System.currentTimeMillis();
	private final double ns = 1000000000.0 / 60.0;
	private double delta = 0.0;

	private boolean pause = false;
	private int pauseTime = 0;

	private void update() {
		long now = System.nanoTime();
		this.delta += (now - lastTime) / ns;
		lastTime = now;
	}

	/**
	 * Use in a while loop!
	 * 
	 * @return boolean variable if the function is allowed to update
	 */
	public boolean continueUpdate() {
		boolean output = false;
		update();
		if (this.delta >= 1) {
			this.delta--;
			output = true;
		}
		if (System.currentTimeMillis() - timer > 1000) {
			timer += 1000;
		}
		if (pause) {
			if (output) {
				if (pauseTime > 1) pauseTime -= 1;
				if (pauseTime == 1) resume();
			}
			return false;
		}
		return output;
	}

	public void changedState() {
		pause(100);
	}

	public void pause() {
		pause(0);
	}

	public void pause(int length) {
		pauseTime = length;
		pause = true;
	}

	public void resume() {
		pauseTime = 0;
		pause = false;
	}
}
