package net.roryclaasen.lost.toolbox;

public class TileCords {
	public static Vector2i getRenderCords(int x, int y) {
		return new Vector2i(x * 32, y * 32);
	}

	public static Vector2i getCordsFromRender(int x, int y) {
		return new Vector2i((int) Math.floor(x / 32), (int) Math.floor(y / 32));
	}

	public static double getDistance(Vector2i tile, Vector2i goal) {
		double dx = tile.getX() - goal.getX();
		double dy = tile.getY() - goal.getY();
		return Math.sqrt(dx * dx + dy * dy);
	}

	public static boolean equals(TileCords cords0, TileCords cords1) {
		if ((cords0.getX() == cords1.getX()) && (cords0.getY() == cords1.getY())) return true;
		return false;
	}

	public boolean equals(TileCords cords) {
		return equals(this, cords);
	}

	private int x, y;

	public TileCords(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public TileCords(Vector2i vector) {
		this.x = vector.getX();
		this.y = vector.getY();
	}

	public Vector2i getRenderCords() {
		return new Vector2i(x * 32, y * 32);
	}

	public Vector2i getCords() {
		return new Vector2i(x, y);
	}

	public String text() {
		return "[x=" + x + ", y=" + y + "]";
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
