package net.roryclaasen.lost.state;

import net.roryclaasen.Bootstrap;
import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.handler.HandlerKeyboard;
import net.roryclaasen.lost.state.States.State;
import net.roryclaasen.lost.toolbox.Arguments;
import net.roryclaasen.lost.toolbox.ui.VideoPlayer;
import net.roryclaasen.lost.window.DisplayWindow;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class IntroState extends ScreenState {

	private int time;

	private VideoPlayer videoPlayer;

	private int stage = -1;
	private final int video1 = 0;
	private final int video2 = 1;
	private final int exit = 2;

	public IntroState(GameThread game, int width, int height, int id) {
		super(game, width, height, id);

	}

	public void start() {
		if (Arguments.doIntro()) {
			if (Bootstrap.isJavaFx() && videoPlayer != null) {
				videoPlayer.play();
				DisplayWindow.setVideo(videoPlayer);

				stage = this.video1;
			}
		}
		if (stage == -1) stage = exit;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		if (Arguments.doIntro()) {
			if (Bootstrap.isJavaFx()) {
				try {
					videoPlayer = new VideoPlayer("assets/video/roryclaasen.mp4");
					videoPlayer.setScale(0.75);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		while (this.game.updateLimiter.continueUpdate()) {
			if (stage == -1) start();
			time++;
			if (stage == video1) {
				videoPlayer.update();
				if (/* time > 8 * 60 || */!videoPlayer.isPlaying() || HandlerKeyboard.space || videoPlayer.getMediaPlayer().getCurrentTime().toMillis() >= 6485.333299999999) {
					videoPlayer.stop();
					videoPlayer.play("assets/video/gogo98901.mp4");
					stage = video2;
					time = 0;
				}
			}
			if (stage == video2) {
				videoPlayer.update();
				if (!videoPlayer.isPlaying() || (HandlerKeyboard.space && time > 20) || videoPlayer.getMediaPlayer().getCurrentTime().toMillis() >= 8234.6667) {
					DisplayWindow.removeVideo(videoPlayer);
					stage = exit;
					time = 0;
				}
			}
			if (stage == exit) {
				states.switchTo(State.MENU);
				stage = exit + 1;
			}
		}
	}
}
