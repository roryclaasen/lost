package net.roryclaasen.lost.state;

import java.util.ArrayList;
import java.util.List;

import net.gogo98901.log.Log;
import net.roryclaasen.Bootstrap;
import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.event.Button.ButtonEvent;
import net.roryclaasen.lost.event.Button.ButtonEventListener;
import net.roryclaasen.lost.graphics.ui.BackgroundImage;
import net.roryclaasen.lost.graphics.ui.Button;
import net.roryclaasen.lost.state.menu.AboutScreen;
import net.roryclaasen.lost.state.menu.HomeScreen;
import net.roryclaasen.lost.state.menu.MenuScreen;
import net.roryclaasen.lost.state.menu.OptionScreen;
import net.roryclaasen.lost.toolbox.list.Fonts;
import net.roryclaasen.lost.toolbox.list.Sites;
import net.roryclaasen.lost.toolbox.list.Strings;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class MenuState extends ScreenState {

	private MenuScreen current, home, option, about;

	private Button version;

	public static final int MAX_PAGES = 3;
	public static final int HOME = 0;
	public static final int OPTION = 1;
	public static final int ABOUT = 2;
	private int page;

	// private BufferedImage background;
	// private BufferedImage menu, options;

	private BackgroundImage background;

	public MenuState(GameThread game, int width, int height, int id) {
		super(game, width, height, id);

		home = new HomeScreen(this);
		option = new OptionScreen(this);
		about = new AboutScreen(this);

		List<Image> images = new ArrayList<Image>();
		try {
			images.add(new Image("assets/images/image0.png"));
			images.add(new Image("assets/images/image1.png"));
			images.add(new Image("assets/images/image2.png"));
		} catch (Exception e) {
			Log.warn("Can't find background image");
		}
		background = new BackgroundImage((width - 600) / 2, 0, 600, 600, images);
		background.setLength(5);
	}

	public void changePageTo(int page) {
		if (page < 0 || page >= MAX_PAGES) return;
		if (page == HOME) current = home;
		if (page == OPTION) current = option;
		if (page == ABOUT) current = about;
		this.page = page;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		home.init();
		option.init();
		about.init();

		current = home;

		version = new Button(Strings._build + "." + Bootstrap.getCommitNumber(), 0, 0);
		version.setFont(Fonts.square24);
		version.setBounds(width - 118, height - 25, 105, 25);
		version.setBackgroundVisible(false);
		version.setTextSize(20F);
		version.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				Sites.github.open("commits");
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		background.render(g);
		if (page == HOME || page == ABOUT) version.render(g);
		current.render(g);
		current.renderUIComponents(g);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		while (this.game.updateLimiter.continueUpdate()) {
			current.updateUIComponents();
			current.update();

			background.update();

			version.update();
		}
	}
}
