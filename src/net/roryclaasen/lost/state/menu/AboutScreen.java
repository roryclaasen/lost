package net.roryclaasen.lost.state.menu;

import net.roryclaasen.lost.event.Button.ButtonEvent;
import net.roryclaasen.lost.event.Button.ButtonEventListener;
import net.roryclaasen.lost.graphics.ui.Button;
import net.roryclaasen.lost.state.MenuState;
import net.roryclaasen.lost.toolbox.list.Fonts;
import net.roryclaasen.lost.toolbox.list.Sites;
import net.roryclaasen.lost.toolbox.list.Strings;

import org.newdawn.slick.Graphics;

public class AboutScreen extends MenuScreen {

	private Button back, website, github;

	public AboutScreen(MenuState state) {
		super(state);
	}

	@Override
	public void init() {
		back = new Button(Strings.menuBack, 20, parent.getHeight() - 60);
		back.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				parent.changePageTo(MenuState.HOME);
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(back);

		github = new Button(Strings.menuGithub, 20, back.getY() - back.getHeight() - 20);
		github.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				Sites.github.open();
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(github);

		website = new Button(Strings.menuWebsite, 20, github.getY() - github.getHeight() - 20);
		website.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				Sites.website.open();
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(website);
	}

	@Override
	public void update() {}

	@Override
	public void render(Graphics g) {
		Fonts.drawCenteredString(g, Strings._title, Fonts.square60, 0, 20, parent.getWidth(), 50);
		Fonts.drawCenteredString(g, "By " + Strings._author, Fonts.square30, 0, 70, parent.getWidth(), 30);

		Fonts.drawCenteredString(g, Strings.aboutTitle, Fonts.square40, 0, 120, parent.getWidth(), 30);

		Fonts.drawFitString(g, Fonts.square24, Strings.aboutText, 20, 200, parent.getWidth() - 40, parent.getHeight() - 200);
	}
}
