package net.roryclaasen.lost.state.menu;

import net.roryclaasen.lost.event.Button.ButtonEvent;
import net.roryclaasen.lost.event.Button.ButtonEventListener;
import net.roryclaasen.lost.graphics.ui.Button;
import net.roryclaasen.lost.state.MenuState;
import net.roryclaasen.lost.state.States.State;
import net.roryclaasen.lost.toolbox.list.Fonts;
import net.roryclaasen.lost.toolbox.list.Strings;

import org.newdawn.slick.Graphics;

public class HomeScreen extends MenuScreen {

	private Button play, options, about, quit;

	public HomeScreen(MenuState state) {
		super(state);
	}

	@Override
	public void init() {
		quit = new Button(Strings.menuQuit, 20, parent.getHeight() - 60);
		quit.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				parent.getGame().callCloseEvent();
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(quit);

		about = new Button(Strings.menuAbout, 20, quit.getY() - quit.getHeight() - 20);
		about.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				parent.changePageTo(MenuState.ABOUT);
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(about);

		options = new Button(Strings.menuOption, 20, about.getY() - about.getHeight() - 20);
		options.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				parent.changePageTo(MenuState.OPTION);
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(options);

		play = new Button(Strings.menuPlay, 20, options.getY() - options.getHeight() - 20);
		play.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				parent.getStates().switchTo(State.GAME);
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(play);
	}

	@Override
	public void update() {}

	@Override
	public void render(Graphics g) {
		Fonts.drawCenteredString(g, Strings._title, Fonts.square60, 0, 20, parent.getWidth(), 50);
		Fonts.drawCenteredString(g, "By " + Strings._author, Fonts.square30, 0, 70, parent.getWidth(), 30);
	}
}
