package net.roryclaasen.lost.state.menu;

import java.util.ArrayList;
import java.util.List;

import net.roryclaasen.lost.graphics.ui.uiBaseObject;
import net.roryclaasen.lost.state.MenuState;

import org.newdawn.slick.Graphics;

public abstract class MenuScreen {

	protected MenuState parent;
	protected List<uiBaseObject> uiItems;

	public MenuScreen(MenuState menuState) {
		this.parent = menuState;

		uiItems = new ArrayList<uiBaseObject>();
	}

	public abstract void init();

	public abstract void update();

	public abstract void render(Graphics g);

	public void updateUIComponents() {
		for (uiBaseObject obj : uiItems) {
			obj.update();
		}
	}

	public void renderUIComponents(Graphics g) {
		for (uiBaseObject obj : uiItems) {
			obj.render(g);
		}
	}
}
