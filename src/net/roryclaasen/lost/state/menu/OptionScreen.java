package net.roryclaasen.lost.state.menu;

import net.gogo98901.util.Data;
import net.roryclaasen.lost.event.Button.ButtonEvent;
import net.roryclaasen.lost.event.Button.ButtonEventListener;
import net.roryclaasen.lost.graphics.ui.Button;
import net.roryclaasen.lost.graphics.ui.ButtonToggle;
import net.roryclaasen.lost.options.Options;
import net.roryclaasen.lost.state.MenuState;
import net.roryclaasen.lost.toolbox.list.Fonts;
import net.roryclaasen.lost.toolbox.list.Strings;

import org.newdawn.slick.Graphics;

public class OptionScreen extends MenuScreen {

	private Button back, reset;
	private ButtonToggle music, sounds;

	private int margin = 20;

	public OptionScreen(MenuState state) {
		super(state);
	}

	@Override
	public void init() {
		back = new Button(Strings.menuBack, margin, parent.getHeight() - 60);
		back.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				parent.changePageTo(MenuState.HOME);
				parent.getGame().options.save();
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(back);

		reset = new Button(Strings.menuReset, margin + back.getWidth() + margin, parent.getHeight() - 60);
		reset.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				music.setValue(music.getDefultValue());
				sounds.setValue(sounds.getDefultValue());

				music.callClickListener();
				sounds.callClickListener();
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(reset);

		music = new ButtonToggle(new String[]{"Music: Off", "Music: On"}, margin, 100, 1);
		music.setValue(Data.booleanToInt(Options.doMusic.get()));
		music.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				Options.doMusic.setData(Data.intToBoolean(music.getValue()));
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(music);

		sounds = new ButtonToggle(new String[]{"Sounds: Off", "Sounds: On"}, margin, 145, 1);
		sounds.setValue(Data.booleanToInt(Options.doSounds.get()));
		sounds.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				Options.doSounds.setData(Data.intToBoolean(sounds.getValue()));
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(sounds);
	}

	@Override
	public void update() {
		back.update();
		reset.update();

		music.update();
		sounds.update();
	}

	@Override
	public void render(Graphics g) {
		Fonts.drawCenteredString(g, Strings.menuOption, Fonts.square30, 0, 20, parent.getWidth(), 30);
		back.render(g);
		reset.render(g);

		music.render(g);
		sounds.render(g);
	}
}
