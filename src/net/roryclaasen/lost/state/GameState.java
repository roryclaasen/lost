package net.roryclaasen.lost.state;

import net.gogo98901.log.Log;
import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.event.Button.ButtonEvent;
import net.roryclaasen.lost.event.Button.ButtonEventListener;
import net.roryclaasen.lost.graphics.ui.Button;
import net.roryclaasen.lost.graphics.ui.UserInterface;
import net.roryclaasen.lost.graphics.ui.uiBaseObject;
import net.roryclaasen.lost.handler.HandlerKeyboard;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.state.States.State;
import net.roryclaasen.lost.toolbox.list.Colors;
import net.roryclaasen.lost.toolbox.list.Files;
import net.roryclaasen.lost.toolbox.list.Fonts;
import net.roryclaasen.lost.toolbox.list.Strings;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class GameState extends ScreenState {

	private Level level;
	private UserInterface ui;

	private int reLoad = 0;

	private boolean pause;

	private Button exit, resume;

	public GameState(GameThread game, int width, int height, int id) {
		super(game, width, height, id);
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		try {
			level = new Level(this);
			ui = new UserInterface(level);
		} catch (Exception e) {
			Log.severe("Canvas+ Initialization... FAILED");
			Log.stackTrace(net.gogo98901.log.Level.SEVERE, e);
			return;
		}
		exit = new Button(Strings.menuExit, 40, height - 100);
		exit.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				states.switchTo(State.MENU);
				pause = false;
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(exit);

		resume = new Button(Strings.menuResume, width - exit.getWidth() - 40, height - 100);
		resume.addEventListener(new ButtonEventListener() {

			@Override
			public void buttonClick(ButtonEvent evt) {
				resume();
			}

			@Override
			public void buttonDragged(ButtonEvent evt) {}

			@Override
			public void buttonPress(ButtonEvent evt) {}
		});
		uiItems.add(resume);
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		level.render(g);
		ui.render(g);

		if (pause) {
			g.setColor(Colors.setOpacity(Color.black, 150));
			g.fillRect(0, 0, width, height);

			for (uiBaseObject obj : uiItems) {
				obj.render(g);
			}

			Fonts.drawCenteredString(g, Strings._title, Fonts.square60, 0, 20, width, 50);
			Fonts.drawCenteredString(g, "PAUSED", Fonts.square30, 0, 70, width, 30);
		}
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		while (this.game.updateLimiter.continueUpdate()) {
			level.update();
			ui.update();
			this.game.canvas.update(container, delta);
			if (!pause) {
				{// General Key Presses
					if (HandlerKeyboard.F7) {
						if (HandlerKeyboard.alt) reLoad = 2;
						else reLoad = 1;
					} else if (reLoad != 0) {
						if (reLoad == 2) level.reLoadLevel(true);
						else level.reLoadLevel(false);
						reLoad = 0;
					}
					if (HandlerKeyboard.esc) pause();
				}
			} else {
				for (uiBaseObject obj : uiItems) {
					obj.update();
				}
			}
		}
	}

	public void start() {
		level.loadLevel(Files.saveLevel1Folder);
		ui.init();
	}

	public UserInterface getUserInterface() {
		return ui;
	}

	public Level getLevel() {
		return level;
	}

	public void pause() {
		Log.info("Game Paused");
		game.soundManager.stopSounds();
		pause = true;
	}

	public void resume() {
		Log.info("Game Resumed");
		pause = false;
	}

	public boolean isPaused() {
		return pause;
	}
}
