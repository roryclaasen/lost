package net.roryclaasen.lost.state;

import java.util.ArrayList;
import java.util.List;

import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.graphics.ui.uiBaseObject;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public abstract class ScreenState extends BasicGameState {

	protected States states;
	protected GameThread game;
	protected int width, height, id;

	protected List<uiBaseObject> uiItems;

	public ScreenState(GameThread game, int width, int height, int id) {
		this.states = game.canvas.states;
		this.game = game;
		this.width = width;
		this.height = height;
		this.id = id;

		uiItems = new ArrayList<uiBaseObject>();
	}

	@Override
	public abstract void init(GameContainer container, StateBasedGame game) throws SlickException;

	@Override
	public abstract void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException;

	@Override
	public abstract void update(GameContainer container, StateBasedGame game, int delta) throws SlickException;

	@Override
	public int getID() {
		return id;
	}

	public States getStates() {
		return states;
	}

	public GameThread getGame() {
		return game;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}
