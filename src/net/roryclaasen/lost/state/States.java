package net.roryclaasen.lost.state;

import net.gogo98901.log.Log;
import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.window.DisplayWindow;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class States {

	private GameThread game;
	private int width, height;

	public enum State {
		INTRO, MENU, GAME
	}

	private State current = State.INTRO;

	public States(GameThread game, int width, int height) {
		this.game = game;
		this.width = width;
		this.height = height;
	}

	public void init(GameContainer container, GameThread game) {
		DisplayWindow.setUpGameContainer(container);
		game.addState(new IntroState(game, width, height, State.INTRO.ordinal()));
		game.addState(new MenuState(game, width, height, State.MENU.ordinal()));
		game.addState(new GameState(game, width, height, State.GAME.ordinal()));
	}

	public void start() {
		// if (!Arguments.doIntro()) switchTo(State.MENU);
	}

	public void switchTo(State state) {
		Log.info("Changing game state to [" + state + "] from [" + current + "]");
		game.updateLimiter.changedState();
		game.enterState(state.ordinal(), new FadeOutTransition(), new FadeInTransition());
		
		@SuppressWarnings("unused")
		ScreenState last = (ScreenState) game.getState(current.ordinal());
		ScreenState next = (ScreenState) game.getState(state.ordinal());

		if (state == State.GAME) {
			((GameState) next).start();
		}
		current = state;
	}

	public boolean is(State state) {
		return current == state;
	}

	public GameState getGameState() {
		return (GameState) game.getState(State.GAME.ordinal());
	}
}
