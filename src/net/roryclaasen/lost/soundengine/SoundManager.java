package net.roryclaasen.lost.soundengine;

import java.util.ArrayList;
import java.util.List;

import net.gogo98901.log.Log;
import net.gogo98901.util.Loader;
import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.options.Options;
import net.roryclaasen.lost.state.States.State;

import org.newdawn.slick.Music;

public class SoundManager {

	public static final int NUMBER_OF_CHANELS = 5;

	private GameThread game;
	private static List<Sound> sounds;
	private static List<Music> musics;

	public static Sound footstep01;

	public SoundManager(GameThread game) {
		this.game = game;
		sounds = new ArrayList<Sound>();
		musics = new ArrayList<Music>();
		loadAll();
	}

	private void loadAll() {
		Log.info("Starting to load audio");
		footstep01 = loadSound("footstep-01.wav");
	}

	@SuppressWarnings("unused")
	private Music loadMusic(String name) {
		Music music = null;
		try {
			music = new Music(Loader.getResource("/assets/sounds/music/" + name));
			musics.add(music);
			Log.info("Music loaded '" + name + "'");
		} catch (Exception e) {
			Log.warn("Music not loaded '" + name + "'");
			Log.stackTrace(e);
		}
		return music;
	}

	private Sound loadSound(String name) {
		Sound sound = null;
		try {
			sound = new Sound("assets/sounds/sound/" + name);
			sounds.add(sound);
			Log.info("Sound loaded '" + name + "'");
		} catch (Exception e) {
			Log.warn("Sound not loaded '" + name + "'");
			Log.stackTrace(e);
		}
		return sound;
	}

	public void update() {
		if (game.canvas.states.is(State.GAME)) {
			if (game.canvas.states.getGameState().isPaused()) {
				// stopSounds();
			}
		}
	}

	public static void play(Music music) {
		play(music, 1F, false);
	}

	public static void play(Music music, float vloume) {
		play(music, vloume, false);
	}

	public static void play(Music music, float volume, boolean loop) {
		if (Options.doMusic.get()) {
			if (musics.contains(music)) {
				if (music != null) {
					if (loop) music.loop(1F, volume);
					else music.play(1F, volume);
				}
			}
		}
	}

	public static void play(Sound sound) {
		play(sound, 1F, 0F, 0F);
	}

	public static void play(Sound sound, float volume) {
		play(sound, volume, 0F, 0F);
	}

	public static void play(Sound sound, float x, float y) {
		play(sound, 1F, x, y);
	}

	public static void play(Sound sound, float volume, float x, float y) {
		playIn(sound, -1, volume, x, y);
	}

	public static void playIn(Sound sound, int id) {
		playIn(sound, id, 1F, 0F, 0F);
	}

	public static void playIn(Sound sound, int id, float volume) {
		playIn(sound, id, volume, 0F, 0F);
	}

	public static void playIn(Sound sound, int id, float x, float y) {
		playIn(sound, id, 1F, x, y);
	}

	public static void playIn(Sound sound, int id, float volume, float x, float y) {
		if (Options.doSounds.get()) {
			if (sounds.contains(sound)) {
				if (sound != null) {
					if (id == -1) sound.play(volume, x, y);
					else sound.play(id, volume, x, y);
				}
			}
		}
	}

	public void stopAll() {
		stopSounds();
		stopMusics();
	}

	public void stopSounds() {
		Log.info("Stopping Sounds");
		for (Sound sound : sounds) {
			sound.stop();
		}
	}

	public void stopMusics() {
		Log.info("Stopping Musics");
		for (Music music : musics) {
			if (music.playing()) music.stop();
		}
	}

	public static List<Sound> getSounds() {
		return sounds;
	}

	public static List<Music> getMusics() {
		return musics;
	}
}
