package net.roryclaasen.lost.soundengine;

import java.io.BufferedInputStream;
import java.io.InputStream;

import net.gogo98901.log.Log;
import net.gogo98901.util.Loader;

import org.newdawn.slick.SlickException;

public class Sound {

	private org.newdawn.slick.Sound[] channel;

	public Sound(String path) throws SlickException {
		channel = new org.newdawn.slick.Sound[SoundManager.NUMBER_OF_CHANELS];
		
		InputStream audioSrc = Loader.getResourceAsStream(path);
		InputStream bufferedIn = new BufferedInputStream(audioSrc);
		
		for (int i = 0; i < channel.length; i++) {
			channel[i] = new org.newdawn.slick.Sound(bufferedIn, path);
		}
	}

	public void play(float volume, float x, float y) {
		play(getFirstSpace(), volume, x, y);
	}

	public void play(int id, float volume, float x, float y) {
		if (id != -1) {
			channel[id].playAt(1F, volume, x, y, 0F);
		} else {
			// Log.warn("Sound can't be played as there are no open channels");
		}
	}

	public void stop() {
		for (org.newdawn.slick.Sound sound : channel) {
			if (sound != null) sound.stop();
		}
	}

	public int getFirstSpace() {
		return getFirstSpace(0);
	}

	public int getFirstSpace(int start) {
		if (start < 0 || start >= channel.length) {
			Log.warn("Start value must sit in 'channel.length'");
			return -1;
		}
		for (int i = start; i < channel.length; i++) {
			if (channel[i] != null) {
				if (!channel[i].playing()) return i;
			}
		}
		return -1;
	}

	public boolean playing(int id) {
		org.newdawn.slick.Sound sound = channel[id];
		if (sound == null) return false;
		return sound.playing();
	}
}
