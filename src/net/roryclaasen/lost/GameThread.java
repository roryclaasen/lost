package net.roryclaasen.lost;

import java.awt.event.WindowEvent;

import net.gogo98901.log.Level;
import net.gogo98901.log.Log;
//import net.roryclaasen.Bootstrap;
import net.roryclaasen.lost.handler.Handler;
import net.roryclaasen.lost.options.Options;
import net.roryclaasen.lost.soundengine.SoundManager;
import net.roryclaasen.lost.state.States;
import net.roryclaasen.lost.toolbox.UpdateLimiter;
import net.roryclaasen.lost.toolbox.list.Files;
import net.roryclaasen.lost.toolbox.list.Strings;
import net.roryclaasen.lost.window.DisplayWindow;
import net.roryclaasen.lost.window.GameCanvas;

import org.lwjgl.openal.AL;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class GameThread extends StateBasedGame {

	@SuppressWarnings("unused")
	private boolean running = false;
	private static boolean started = false;

	public Options options;
	@SuppressWarnings("unused")
	private DisplayWindow window;
	public GameThread game;
	public GameCanvas canvas;
	private GameChecker checker;

	public Handler handler;
	public SoundManager soundManager;

	public int currentFrames, currentUpdates;
	private int time;

	public UpdateLimiter updateLimiter;

	public GameThread(DisplayWindow window) {
		super(Strings._title + " by " + Strings._author);
		game = this;
		this.window = window;

		canvas = new GameCanvas(game, DisplayWindow.getWidth(), DisplayWindow.getHeight());
	}

	public void init() {
		try {
			options = new Options();

			canvas.init();
			// window.addComponents(canvas);

			soundManager = new SoundManager(this);

			checker = new GameChecker(game);

			handler = new Handler(game);

			updateLimiter = new UpdateLimiter();
		} catch (Exception e) {
			Log.severe("Game Initialization... FAILED");
			Log.stackTrace(Level.SEVERE, e);
		}
		Log.info("Game Initialization... OKAY");
	}

	public void start() {
		Log.info("Starting Game!");
		running = true;
		try {
			// TODO Add Game Peripherals
		} catch (Exception e) {
			Log.severe("Game Peripherals... FAILED");
			Log.stackTrace(Level.SEVERE, e);
			callCloseEvent();
		}

		Log.info("Game Peripherals... OKAY");
		started = true;
		checker.start();
		canvas.states.start();
		Log.info("Game has started!");
	}

	public void stop() {
		Log.info("Stopping Game!");
		running = false;
		checker.stop();

		callCloseEvent();
	}

	public void callCloseEvent() {
		DisplayWindow.getFrame().dispatchEvent(new WindowEvent(DisplayWindow.getFrame(), WindowEvent.WINDOW_CLOSING));
	}

	public void closeFunction() {
		Log.info("Window Closing");
		Log.info("Stopping functions");
		soundManager.stopAll();

		if (canvas.states.is(States.State.GAME)) {
			canvas.states.getGameState().pause();
		}
		Log.info("Saving files");
		options.save();
		Log.save(Files.dataFolder.getAbsolutePath().toString(), "latest.log");
		Log.info("Close sequence finished");
		AL.destroy();
		Display.destroy();
		// System.exit(0);
	}

	public int time() {
		return time;
	}

	public int getCurrentFrames() {
		return currentFrames;
	}

	public int getCurrentUpdates() {
		return currentUpdates;
	}

	public static boolean hasStarted() {
		return started;
	}

	@Deprecated
	public void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			Log.stackTrace(e);
		}
	}

	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		canvas.states.init(container, this);

		handler.addAll(container);
	}
}
