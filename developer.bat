@echo off
title Lost developer command

echo *********************************************************************
echo *                                                                   *
echo *                      Lost (developer program)                     *
echo *                       A game by Rory Claasen                      *
echo *                                                                   *
echo *********************************************************************

echo * INFO: Searching for git.exe...                                    *
where git >nul 2>&1
IF %ERRORLEVEL% NEQ 0 (
	echo * ERROR: git.exe was not found                                      *
	echo *********************************************************************
	if not "%1" == "game" (pause)
	goto :eof
)
echo * INFO: Git has been found                                          *

echo *********************************************************************

echo * INFO: checking if folder can be found                             *
if EXIST res (
	echo * INFO: Output folder has been found                                *
) ELSE (
	echo * WARNING: Output folder was not found!                             *
	echo *********************************************************************
	IF not "%1" == "game" (pause)
	goto :eof
)

echo *********************************************************************

echo * INFO: Running git commands...                                     *
git rev-parse --short HEAD > res/version
git rev-list HEAD --count > res/number
echo * INFO: git rev-parse --short HEAD                                  *
echo * INFO: git rev-list HEAD --count                                   *
echo * INFO: Program finnished                                           *

echo *********************************************************************
IF "%1" == "game" (goto :eof)
pause
